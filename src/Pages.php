<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/8/2019
 * Time: 2:16 PM
 */

namespace App;


class Pages extends Database
{
    private $id;
    private $title;
    private $description;
    private $link;

    /**
     * @param mixed $phone
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
    public function setLink($link)
    {
        $this->link = $link;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getLink()
    {
        return $this->link;
    }
    /**
     * @return mixed
     */

    public function getId()
    {
        return $this->id;
    }
    public function insertData($title, $description,$link,$table){

        $sql = "INSERT INTO $table SET title=:title, description=:description, link=:link";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':title'=>$title, ':description'=>$description, ':link'=>$link));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id, $title, $description,$link,$table){

        $sql = "UPDATE $table
 SET title=:title, description=:description, link=:link  WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':title'=>$title, ':description'=>$description, ':link'=>$link ));
        return true;

    }
}