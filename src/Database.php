<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 7/4/2019
 * Time: 2:47 PM
 */

namespace App;

use PDO;


class Database
{


    private $host="localhost";


//    private $user="id10248660_invoice";
//    private $db="id10248660_students_payment";
//    private $pass="13579";


    private $user="root";
    private $db="ebook";
    private $pass="";


    public $conn;

    public function __construct(){

        $this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->db,$this->user,$this->pass);
    }

    public function showData($table){

        $sql="SELECT * FROM $table ORDER BY id DESC";
        $q = $this->conn->query($sql) or die("failed!");

        while($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$r;
        }
        return $data;
    }

    public function getById($id,$table){

        $sql="SELECT * FROM $table WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        return $data;
    }



    public function deleteData($id,$table){

        $sql="DELETE FROM $table WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id));
        return true;
    }

    public function serverDateTime()
    {
        $data = date("Y-m-d h:i:s", time()) ;
        return $data;
    }

    public function countRows($table)
    {
        $sql = "SELECT COUNT(id) as id FROM $table";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC))
        {
            $data = $r['id'];
        }
        return $data;
    }



}