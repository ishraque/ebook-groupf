<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/4/2019
 * Time: 3:23 PM
 */

namespace App;

use PDO;

class Users extends  Database
{
    public  $table = "users";

    public function insertData($data)
    {
        extract($data);
        $sql = "INSERT INTO users SET 
username=:username,
PASS=:PASS,
email=:email
";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(":username"=>$username,
            ":PASS"=>$PASS,
            ":email"=>$email));

        if($r)
            return true;
        else
            return false;
    }

    public function updateData($data)
    {
        extract($data);
        $sql = "UPDATE users SET
username=:username,
PASS=:PASS,
email=:email WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
            ":id"=>$id,
            ":username"=>$username,
            ":PASS"=>$PASS,
            ":email"=>$email));
        if($r)
            return true;
        else
            return false;
    }

//    Login method That returns User ID
    public function login($data)
    {
        extract($data);
        $sql = "SELECT * FROM users WHERE
username=:username AND
PASS=:PASS";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
            ":username"=>$username,
            ":PASS"=>$PASS
        ));


        if($q->rowCount() > 0)
        {
            while ($row = $q->fetch(PDO::FETCH_ASSOC))
            {
                $id = $row['id'];
            }
        }


        if(isset($id))
            return $id;
        else
            return false;

    }
}