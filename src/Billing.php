<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/15/2019
 * Time: 9:53 PM
 */

namespace App;
use PDO;

class Billing extends Database
{
    public $table = "billing";

    public function insertData($order_id, $data)
    {
        $purchase_date = date("Y-m-d") ;
        extract($data);
        $sql = "INSERT INTO billing SET 
                        order_id=:order_id,
                        firstName=:firstName,
                        lastName=:lastName,
                        companyName=:companyName,
                        country=:country,
                        address=:address,
                        district=:district,
                        zip=:zip,
                        phone=:phone,
                        email=:email,
                        purchase_date=:purchase_date";
        $q = $this->conn->prepare($sql);
       $r = $q->execute(array(
           ":order_id" => $order_id,
            ":firstName"=>$firstName,
                       ":lastName"=>$lastName,
                        ":companyName"=>$companyName,
                       ":country"=>$country,
                       ":address"=>$address,
                       ":district"=>$district,
                      ":zip"=>$zip,
                      ":phone"=>$phone,
                      ":email"=>$email,
                      ":purchase_date"=>$purchase_date
        ));
       if($r)
           return true;
       else
           return false;
    }


    public function getBillByOrderId($order_id)
    {
        $sql = "SELECT * FROM billing WHERE order_id = $order_id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute();
        while($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data = $r;
        }
        return $data;
    }

    public function deleteBillByOrderId($order_id)
    {
        $sql = "DELETE FROM billing WHERE order_id = $order_id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute();
        if($r)
            return true;
        else
            return false;
    }

}