<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/28/2019
 * Time: 10:43 AM
 */

namespace App;
use PDO;

class Products extends Database
{
    public $table = "products";


    public function insertData($data)
    {
        extract($data);
        extract($_POST);


//        $picture = "absd.jpj";

        $created_at = date("Y-m-d h:i:s", time()) ;

//image Uploading COde
//image Uploading COde
//image Uploading COde
//image Uploading COde


        $target_dir = "uploads/";

        $sql ="SELECT id FROM products ORDER BY id DESC LIMIT 0,1 ";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($row = $q->fetch(PDO::FETCH_ASSOC))
        {
            $last_id = $row['id'];
        }

        $last_id += 1;

        //$picture = md5($last_id.$title);

        $ext= explode(".",$_FILES['picture']['name']);
        $c=count($ext);
        $ext=$ext[$c-1];
        $image_name=crc32($last_id.$title);
        $picture = $image_name.".".$ext;




        $target_file = $target_dir . $picture;

        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["picture"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
//        if ($_FILES["picture"]["size"] > 500000) {
//            echo "Sorry, your file is too large.";
//            $uploadOk = 0;
//        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["picture"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }










//image Uploading COde
//image Uploading COde
//image Uploading COde
//image Uploading COde
//image Uploading COde
//image Uploading COde
//image Uploading COde






  $sql = "INSERT INTO products(
                     title,
                     picture,
                     short_description,
                     description,
                     brand_id,
                     label_id,
                     total_sales,
                     product_type,
                     is_new,
                     cost,
                     mrp,
                     special_price,
                     created_at) VALUES (
                                         :title,
                                         :picture,
                                         :short_description,
                                         :description,
                                         :brand_id,
                                         :label_id,
                                         :total_sales,
                                         :product_type,
                                         :is_new,
                                         :cost,
                                         :mrp,
                                         :special_price,
                                         :created_at
                                         )";
       $q = $this->conn->prepare($sql);
       $r = $q->execute(array(
           ":created_at" => $created_at,
           ":special_price" =>$special_price,
           ":mrp" => $mrp,
           ":cost" => $cost,
           ":is_new" => $is_new,
           ":product_type" => $product_type,
           ":total_sales" => $total_sales,
           ":label_id" => $label_id,
           ":brand_id" => $brand_id,
           ":description" => $description,
           ":short_description" => $short_description,
           ":picture"=> $picture,
           ":title"=>$title
       ));
       if($r)
           return true;
       else
           return false;
    }


    public function updateData($data)
    {
        extract($data);
        $modified_at = date("Y-m-d h:i:s", time()) ;
        $sql = "UPDATE products SET description=:description,
                    title=:title,
                    modified_at=:modified_at,
                    special_price=:special_price,
                    mrp=:mrp,
                    cost=:cost,
                    is_new=:is_new,
                    product_type=:product_type,
                    total_sales=:total_sales,
                    label_id=:label_id,
                    brand_id=:brand_id,
                    short_description=:short_description WHERE id =:id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
            ":id" => $id,
            ":modified_at" => $modified_at,
            ":special_price" => $special_price,
            ":mrp" => $mrp,
            ":cost" => $cost,
            ":is_new" => $is_new,
            ":product_type" => $product_type,
            ":total_sales" => $total_sales,
            ":label_id" => $label_id,
            ":brand_id" => $brand_id,
            ":description" => $description,
            ":short_description" => $short_description,
            ":title"=>$title));
        if($r)
            return true;
        else
            return false;

    }

    public function showByProductType($product_type)
    {
        $sql = "SELECT * FROM products where product_type =:product_type ";
        $q = $this->conn->prepare($sql);
       $q->execute(array(
            ":product_type"=>$product_type
        ));
        while ($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data[] = $r;
        }
        return $data;
    }

    public function showLimitedData($initial, $quantity)
    {
        $sql = "SELECT * FROM products LIMIT $initial, $quantity ";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data[] = $r;
        }
        return $data;
    }

}