<?php


namespace App;


class Contacts extends Database
{
    private $id;
    private $name;
    private $email;
    private $subject;
    private $comment;
    private $status;
    private $soft_delete;
    private $date;

    /**
     * @param mixed $phone
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param mixed $name
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
    /**
     * @return mixed
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    /**
     * @return mixed
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getSoft_delete()
    {
        return $this->soft_delete;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function insertData($name,$email,$subject,$comment,$status,$soft_delete,$date, $created_at, $table){

        $sql = "INSERT INTO $table SET name=:name,email=:email,subject=:subject,comment=:comment,status=:status,soft_delete=:soft_delete,date=:date,created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':name'=>$name,':email'=>$email,':subject'=>$subject,':comment'=>$comment,':status'=>$status,':soft_delete'=>$soft_delete,':date'=>$date,':created_at'=>$created_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id,$name,$email,$subject,$comment,$status,$soft_delete,$date, $updated_at, $table){

        $sql = "UPDATE $table SET name=:name,email=:email,subject=:subject,comment=:comment,status=:status,soft_delete=:soft_delete,date=:date,updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':name'=>$name, ':email'=>$email,':subject'=>$subject,':comment'=>$comment,':status'=>$status,':soft_delete'=>$soft_delete,':date'=>$date,':updated_at'=>$updated_at ));
        return true;

    }
}