<?php


namespace App;


class Subscribers extends Database
{
    private $id;
    private $email;
    private $is_subscribed;
    private $reason;
    /**
     * @param mixed $phone
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $name
     */
    public function setIs_subscribed($is_subscribed)
    {
        $this->is_subscribed = $is_subscribed;
    }

    /**
     * @param mixed $email
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    /**
     * @param mixed $name
     */

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getIs_subscribed()
    {
        return $this->is_subscribed;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function insertData($email,$is_subscribed,$reason, $created_at, $table){

        $sql = "INSERT INTO $table SET email=:email,is_subscribed=:is_subscribed,reason=:reason,created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':email'=>$email,':is_subscribed'=>$is_subscribed,':reason'=>$reason, ':created_at'=>$created_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id,$email,$is_subscribed,$reason, $updated_at, $table){

        $sql = "UPDATE $table SET email=:email,is_subscribed=:is_subscribed,reason=:reason,updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':email'=>$email,':is_subscribed'=>$is_subscribed,':reason'=>$reason,':updated_at'=>$updated_at ));
        return true;

    }
}