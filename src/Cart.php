<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/4/2019
 * Time: 4:53 PM
 */

namespace App;
use PDO;

class Cart extends Database
{

    public  $table = "carts";

    public function addToCart($user_id, $product_id, $qty)
    {

        //checking product is already in cart ?
        $sql = "SELECT total_price, qty, unite_price FROM carts WHERE user_id = $user_id and product_id = $product_id";
        $q = $this->conn->prepare($sql);
        $q->execute();
        if($q->rowCount() >0){

            while ($row = $q->fetch(PDO::FETCH_ASSOC))
            {
                $data = $row;
            }
            $data['qty'] += $qty;
            $data['total_price'] = $data['unite_price'] * $data['qty'];
            $updated_at = date("Y-m-d h:i:s", time()) ;
            extract($data);
            $sql = "UPDATE carts SET
                 
                 qty=:qty,
                 total_price=:total_price,
                 updated_at=:updated_at
                 WHERE user_id=:user_id and product_id=:product_id
                 ";
            $q = $this->conn->prepare($sql);
            $r = $q->execute(array(
                ":qty" => $qty,
                ":total_price" =>$total_price,
                ":updated_at" =>$updated_at,
                ":user_id" => $user_id,
                ":product_id" => $product_id
            ));
            if($r)
               return true;
            else
                false;

        }
        else{

            $sql = "SELECT title as product_title,
            picture,
            cost as unite_price FROM products where id = $product_id";
            $q = $this->conn->prepare($sql);
            $q->execute();


            while ($row = $q->fetch(PDO::FETCH_ASSOC))
            {
                $data =$row;
            }
             extract($data);

            $created_at = date("Y-m-d h:i:s", time()) ;
           $sql = "INSERT INTO carts SET
                                       user_id=$user_id,
                                       product_title = '$product_title',
                                       picture ='$picture',
                                       product_id=$product_id,
                                       qty = $qty,
                                       unite_price = $unite_price,
                                       total_price = $unite_price,
                                       created_at = '$created_at '
                  ";

            $q = $this->conn->prepare($sql);
            $r = $q->execute();
            if($r)
                return true;
            else
                return false;

        }




    }

    //        cart product quantity
    public function showCartAmount($user_id)
    {
        $sql = "SELECT SUM(qty) as qty FROM carts WHERE user_id = $user_id";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($row = $q->fetch(PDO::FETCH_ASSOC))
        {
            $sum = $row['qty'];
        }

            return $sum;

    }

    public function showUserCart($user_id)
    {
        $sql = "SELECT * FROM carts WHERE user_id = $user_id";
        $q = $this->conn->prepare($sql);
        $q->execute();
        if($q->rowCount() > 0){
            while ($row = $q->fetch(PDO::FETCH_ASSOC))
            {
                $data[] = $row;
            }
            return $data;
        }
        else
            return false;


    }


    public function showCartItems($user_id)
    {
        $sql = "SELECT COUNT(id) as items FROM carts WHERE user_id = $user_id";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($row = $q->fetch(PDO::FETCH_ASSOC))
        {
            $items = $row['items'];
        }
        return $items;
    }

    public function cartSubTotal($user_id)
    {
        $sql = "SELECT SUM(total_price) as subtotal FROM carts WHERE user_id = $user_id";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($row = $q->fetch(PDO::FETCH_ASSOC))
        {
            $subtotal = $row['subtotal'];
        }
        return $subtotal;
    }

    public function updateCart($id, $unite_price, $qty)
    {
        $total_price = $unite_price * $qty;
        $sql = "UPDATE carts SET
        qty = $qty,
                 total_price = $total_price
                 WHERE id = $id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute();
        if($r)
            return true;
        else
            return false;

    }
}