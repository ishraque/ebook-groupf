<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/11/2019
 * Time: 1:50 PM
 */

namespace App;
use PDO;

class Categories extends Database
{
    public $table = "categories";

    public function insertData($data)
    {
        extract($data);
        $created_at = date("Y-m-d h:i:s", time()) ;
        $table = $this->table;
        $sql = "INSERT INTO $table SET 
                                       name=:name,
                                       parent=:parent,
                                       created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
            ":created_at"=>$created_at,
            ":name"=>$name,
            ":parent"=>$parent
        ));
        if($r)
            return true;
        else
            return false;

    }


    public function updateCategory($data)
    {
        extract($data);

        $table = $this->table;
        $modified_at = date("Y-m-d h:i:s", time()) ;
       $sql = "UPDATE $table SET name=:name,parent=:parent, modified_at=:modified_at WHERE id = :id";
        $q = $this->conn->prepare($sql);

        $r = $q->execute(array(
            ":id"=>$id,
            ":parent"=>$parent,
            ":name"=>$name,
            ":modified_at"=>$modified_at
        ));
        if($r)
            return true;
        else
            return false;

    }

//    public function showCategories(){
//
//        $sql = "SELECT p.id, p.soft_delete, p.is_draft, p.created_at, p.modified_at, p.name as category,
//       c.name as subcategory FROM categories p, categories c WHERE p.id = c.parent order by p.id DESC ";
//        $q = $this->conn->prepare($sql);
//        $r = $q->execute();
//        while ($row = $q->fetch(PDO::FETCH_ASSOC)){
//            $data[] = $row;
//        }
//        return $data;
//    }

    public function countCategories()
    {
        $sql = "SELECT COUNT(id) as total_categories FROM categories";
        $q = $this->conn->prepare($sql);
        $r = $q->execute();
        while ($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data = $row['total_categories'];
        }
        return $data;

    }

    public function countProductsByName($name)
    {
        $sql = "SELECT COUNT(id) as sum_p FROM products WHERE product_type =:name";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(":name"=>$name));
        while ($row = $q->fetch(PDO::FETCH_ASSOC)){
            $data = $row['sum_p'];
        }
        return $data;

    }

    public function limitedData($initial, $quantity)
    {
        $sql = "SELECT * FROM categories ORDER BY id DESC LIMIT $initial,$quantity ";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC)){
      $data[] = $r;
        }
        return $data;
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";


    }

}