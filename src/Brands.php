<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/8/2019
 * Time: 2:16 PM
 */

namespace App;


class Brands extends Database
{
    private $id;
    private $title;
    private $link;
    private $is_draft;
    private $is_active;
    private $soft_delete;
    private $created_at;
    private $modified_at;

    /**
     * @param mixed $phone
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setLink($link)
    {
        $this->link = $link;
    }
    public function setIs_draft($is_draft)
    {
        $this->is_draft = $is_draft;
    }

    public function setIs_active($is_active)
    {
        $this->is_active = $is_active;
    }
    public function setSoft_delete($soft_delete)
    {
        $this->soft_delete = $soft_delete;
    }
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }
    public function setModified_at($modified_at)
    {
        $this->modified_at = $modified_at;
    }


    public function getTitle()
    {
        return $this->title;
    }
    public function getLink()
    {
        return $this->link;
    }
    public function getIs_draft()
    {
        return $this->is_draft;
    }
    public function getIs_active()
    {
        return $this->is_active;
    }
    public function getSoft_delete()
    {
        return $this->soft_delete;
    }
    public function getCreated_at()
    {
        return $this->created_at;
    }
    public function getModified_at()
    {
        return $this->modified_at;
    }
    /**
     * @return mixed
     */

    public function getId()
    {
        return $this->id;
    }
    public function insertData($title,$link,$is_draft,$is_active,$soft_delete,$created_at,$modified_at,$table){

        $sql = "INSERT INTO $table SET title=:title,link=:link,is_draft=:is_draft,is_active=:is_active,soft_delete=:soft_delete,created_at=:created_at,modified_at=:modified_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':title'=>$title,':link'=>$link, ':is_draft'=>$is_draft, ':is_active'=>$is_active,':soft_delete'=>$soft_delete,':created_at'=>$created_at,':modified_at'=>$modified_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id, $title,$link,$is_draft,$is_active,$soft_delete,$created_at,$modified_at,$table){

        $sql = "UPDATE $table
 SET title=:title,link=:link,is_draft=:is_draft,is_active=:is_active,soft_delete=:soft_delete,created_at=:created_at,modified_at=:modified_at  WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':title'=>$title,':link'=>$link, ':is_draft'=>$is_draft, ':is_active'=>$is_active,':soft_delete'=>$soft_delete,':created_at'=>$created_at,':modified_at'=>$modified_at));
        return true;

    }
    public function inactivate($id, $table)
    {

        $sql = "UPDATE $table SET is_active = 0 WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        return true;
    }
    public function activate($id, $table)
    {

        $sql = "UPDATE $table SET is_active = 1 WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        return true;
    }
}