<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/8/2019
 * Time: 2:16 PM
 */

namespace App;


class Tags extends Database
{
    private $id;
    private $title;


    /**
     * @param mixed $phone
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $name
     */

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */

    public function getId()
    {
        return $this->id;
    }
    public function insertData($title, $created_at, $table){

        $sql = "INSERT INTO $table SET title=:title, created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':title'=>$title, ':created_at'=>$created_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id, $title, $updated_at, $table){

        $sql = "UPDATE $table
 SET title=:title, updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':title'=>$title, ':updated_at'=>$updated_at ));
        return true;

    }
}