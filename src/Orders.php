<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/15/2019
 * Time: 6:35 PM
 */

namespace App;
use PDO;

class Orders extends Database
{
    public $table = "orders";
    public function insertData( $order_id, $product_id, $qty, $user_id)
    {

        $purchase_date = date("Y-m-d h:i:s", time()) ;
        $sql = "INSERT INTO orders SET
                       order_id=:order_id,
                       product_id=:product_id,
                       qty=:qty,
                       user_id=:user_id,
                       purchase_date=:purchase_date";
        $q = $this->conn->prepare($sql);
        $r = $q->execute(array(
                ":order_id"=>$order_id,
            ":product_id"=>$product_id,
            ":qty"=>$qty,
            ":user_id"=>$user_id,
            ":purchase_date"=>$purchase_date
        ));
        if($r)
            return true;
        else
            return false;

    }

    public function newOrderId()
    {
        $sql ="SELECT order_id FROM orders ORDER  BY id DESC LIMIT 0, 1";
        $q = $this->conn->prepare($sql);
         $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC))
        {
            $order_id = $r['order_id'];
        }
        $order_id +=1;
        return $order_id;
    }

    public function showOrders($id)
    {
        $sql="SELECT p.title, p.cost, qty FROM orders o, products p WHERE o.product_id=p.id AND o.order_id= $id ORDER BY o.id DESC ";
        $q = $this->conn->query($sql) or die("failed!");

        while($r = $q->fetch(PDO::FETCH_ASSOC)){
            $data[]=$r;
        }
        return $data;
    }

    public function countOrders()
    {
        $sql = "SELECT COUNT(DISTINCT(order_id)) as orders FROM orders";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC))
        {
            $data = $r['orders'] ;
        }

        return $data;
    }

    public function showAllOrders()
    {
        $sql = "SELECT *  FROM orders GROUP BY order_id ORDER BY order_id DESC";
        $q = $this->conn->prepare($sql);
        $q->execute();
        while ($r = $q->fetch(PDO::FETCH_ASSOC))
        {
            $data[] = $r;
        }

        return $data;
    }

    public function deleteOrder($order_id)
    {
        $sql = "DELETE FROM orders WHERE order_id = $order_id";
        $q = $this->conn->prepare($sql);
        $r = $q->execute();
        if($r)
             return true;
        else
            return false;
    }

}