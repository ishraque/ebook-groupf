<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 8/8/2019
 * Time: 2:16 PM
 */

namespace App;


class Labels extends Database
{
    private $id;
    private $title;
    private $picture;

    /**
     * @param mixed $phone
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setPicture($picture)
    {
        $this->title = $picture;
    }

    public function getTitle()
    {
        return $this->title;
    }
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return mixed
     */

    public function getId()
    {
        return $this->id;
    }
    public function insertData($title, $picture, $table){

        $sql = "INSERT INTO $table SET title=:title, picture=:picture";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':title'=>$title, ':picture'=>$picture));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id, $title, $picture, $table){

        $sql = "UPDATE $table
 SET title=:title,picture=:picture  WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':title'=>$title, ':picture'=>$picture ));
        return true;

    }
}