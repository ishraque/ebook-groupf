<?php
namespace App;

class Sponsers extends Database
{

    private $id;
    private $title;
    private $picture;
    private $link;
    private $promotional_message;
    private $html_banner;
    private $is_active;
    private $is_draft;
    private $soft_delete;
    private $created_at;
    private $modified_at;

    /**
     * @param $title
     */


    private function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param $picture
     */
    private function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @param $link
     */
    private function setLink($link)
    {
        $this->link = $link;

    }
    public function setPromotional_message($promotional_message)
    {
        $this->promotional_message = $promotional_message;
    }
    public function setHtml_banner($html_banner)
    {
        $this->html_banner = $html_banner;
    }
    public function setIs_active($is_active)
    {
        $this->is_active = $is_active;
    }
    public function setIs_draft($is_draft)
    {
        $this->is_draft = $is_draft;
    }

    /**
     * @param $soft_delete
     */
    private function setSoft_delete($soft_delete)
    {
        $this->soft_delete = $soft_delete;

    }
    public function setCreated_at($created_at)
    {
        $this->created_at = $created_at;
    }
    public function setModified_at($modified_at)
    {
        $this->modified_at = $modified_at;
    }
    /**
     * @return mixed
     */

    public function getTitle()
    {
        return $this->title;
    }
    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }


    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    public function getPromotional_message()
    {
        return $this->promotional_message;
    }

    /**
     * @return mixed
     */
    public function getHtml_banner()
    {
        return $this->html_banner;
    }
    public function getIs_active()
    {
        return $this->is_active;
    }
    public function getIs_draft()
    {
        return $this->is_draft;
    }
    public function getSoft_delete()
    {
        return $this->soft_delete;
    }


    /**
     * @return mixed
     */
    public function getCreated_at()
    {
        return $this->created_at;
    }
    public function getModified_at()
    {
        return $this->modified_at;
    }
    public function getId()
    {
        return $this->id;
    }
    public function insertData( $title,$picture, $link,$promotional_message,$html_banner,$is_active, $is_draft, $soft_delete,$created_at,$modified_at,$table){

        $sql = "INSERT INTO $table SET title=:title, picture=:picture, link=:link,promotional_message=:promotional_message,html_banner=:html_banner,is_active=:is_active,is_draft=:is_draft,soft_delete=:soft_delete,created_at=:created_at,modified_at=:modified_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':title'=>$title,':picture'=>$picture,':link'=>$link,':promotional_message'=>$promotional_message,':html_banner'=>$html_banner,':is_active'=>$is_active,':is_draft'=>$is_draft,':soft_delete'=>$soft_delete,':created_at'=>$created_at,':modified_at'=>$modified_at));
        if($q)
            return true;
        else
            return false;
    }
    public function update($id,$title,$picture, $link,$promotional_message,$html_banner,$is_active,$table){

        $sql = "UPDATE $table
 SET  title=:title, picture=:picture, link=:link,promotional_message=:promotional_message,
html_banner=:html_banner,is_active=:is_active 
WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':title'=>$title,':picture'=>$picture,':link'=>$link,
            ':promotional_message'=>$promotional_message,':html_banner'=>$html_banner,':is_active'=>$is_active));
        return true;


    }
    public function inactivate($id, $table)
    {

        $sql = "UPDATE $table SET is_active = 0 WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        return true;
    }
    public function activate($id, $table)
    {

        $sql = "UPDATE $table SET is_active = 1 WHERE id = :id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id' => $id));
        return true;
    }
}
