<?php


namespace App;


class Popular_tags extends Database
{
    private $id;
    private $name;
    private $link;
    private $is_active;
    private $soft_delete;
    private $is_draft;

    /**
     * @param mixed $phone
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $email
     */
    public function setIs_active($is_active)
    {
        $this->is_active = $is_active;
    }
    /**
     * @return mixed
     */
    public function setSoft_delete($soft_delete)
    {
        $this->soft_delete = $soft_delete;
    }
    /**
     * @return mixed
     */
    public function setIs_draft($is_draft)
    {
        $this->is_draft = $is_draft;
    }
    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getIs_active()
    {
        return $this->is_active;
    }

    /**
     * @return mixed
     */
    public function getSoft_delete()
    {
        return $this->soft_delete;
    }

    /**
     * @return mixed
     */
    public function getIs_draft()
    {
        return $this->is_draft;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function insertData($name,$link,$is_active,$soft_delete,$is_draft, $created_at, $table){

        $sql = "INSERT INTO $table SET name=:name,link=:link,is_active=:is_active,soft_delete=:soft_delete,is_draft=:is_draft,created_at=:created_at";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':name'=>$name,':link'=>$link,
            ':is_active'=>$is_active,':soft_delete'=>$soft_delete,':is_draft'=>$is_draft,':created_at'=>$created_at));
        if($q)
            return true;
        else
            return false;
    }

    public function update($id,$name,$link,$is_active,$soft_delete,$is_draft, $updated_at, $table){

        $sql = "UPDATE $table
 SET name=:name,link=:link,is_active=:is_active,soft_delete=:soft_delete,is_draft=:is_draft,updated_at=:updated_at WHERE id=:id";
        $q = $this->conn->prepare($sql);
        $q->execute(array(':id'=>$id,':name'=>$name,':link'=>$link,':is_active'=>$is_active,':soft_delete'=>$soft_delete,':is_draft'=>$is_draft,':updated_at'=>$updated_at ));
        return true;

    }
}