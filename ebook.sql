-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 18, 2019 at 08:26 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `max_display` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `max_display`, `created_at`, `modified_at`) VALUES
(1, 'banner1', 'banner-1.jpg', 'Codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-24 00:00:00', '2019-07-25 00:00:00'),
(3, 'Banner 4', 'banner-4.jpg', 'codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-21 00:00:00', '2019-07-21 00:00:00'),
(4, 'Banner 5', 'banner-2.jpg', 'codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-21 00:00:00', '2019-07-21 00:00:00'),
(8, 'Banner6', 'banner-6.jpg', 'Codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-29 00:00:00', '2019-07-29 00:00:00'),
(9, 'Banner7', '1.jpg', 'Codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-29 00:00:00', '2019-07-29 00:00:00'),
(10, 'Banner8', 'banner-7.jpg', 'Codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-30 00:00:00', '2019-07-30 00:00:00'),
(11, 'Banner9', 'banner-5.jpg', 'Codepen', 'Welcome', 'html', 1, 1, 1, 1, '2019-07-30 00:00:00', '2019-07-30 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
CREATE TABLE IF NOT EXISTS `billing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `companyName` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `address` text,
  `district` varchar(50) DEFAULT NULL,
  `zip` int(11) DEFAULT NULL,
  `phone` int(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `soft_delete` tinyint(4) DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `order_id`, `firstName`, `lastName`, `companyName`, `country`, `address`, `district`, `zip`, `phone`, `email`, `soft_delete`, `purchase_date`, `modified_at`) VALUES
(4, 12, 'Ishraqul', 'Hoque', 'Web Development', 'Afghanistan', 'CDA Avenue, 3145 Bari basha, Zakir hossain Road L/A, East Nasirabad', 'Afghanistan', 4000, 1377234234, 'asdasd@gmail.com', NULL, '2019-08-16', NULL),
(7, 13, 'Arafat', 'Hossain', 'Web Development', 'American Samoa', 'CDA Avenue', 'Anguilla', 5242, 32145622, 'asdasd@gmail.com', NULL, '2019-08-16', NULL),
(8, 14, 'Araf', 'Hoque', 'BD foods', 'Anguilla', 'CDA Avenue', 'American Samoa', 4000, 123123, 'asdasd@gmail.com', NULL, '2019-08-16', NULL),
(9, 15, 'Sowad', 'Sadik', 'Hoque foods', 'Afghanistan', 'Zakir Hossain Road L/A, East Nasirabad', 'Afghanistan', 123123, 1377234234, 'asdasd@gmail.com', NULL, '2019-08-16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `link`, `is_draft`, `is_active`, `soft_delete`, `created_at`, `modified_at`) VALUES
(2, 'Somoy', 'somoy.com.bd', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(3, 'Ananna', 'ananna.com.bd', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(4, 'Oitijjho', 'oitijjho.com.bd', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(5, 'Anya Prokash', 'anyaprokash.com.bd', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(6, 'Uthsho', 'uthsho.com.bd', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE IF NOT EXISTS `carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `product_title` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `unite_price` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`, `product_id`, `picture`, `product_title`, `qty`, `unite_price`, `total_price`, `created_at`, `updated_at`) VALUES
(117, 36, 12, '3842888698.jpg', 'The Women In the Window', 2, 56, 112, '2019-08-16 06:25:25', '2019-08-16 06:33:08'),
(126, 36, 13, '757458889.jpg', 'Bad Blood', 1, 88, 88, '2019-08-16 06:36:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent` int(255) NOT NULL DEFAULT '0',
  `soft_delete` tinyint(1) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(38, 'Biography', 40, NULL, NULL, '2019-08-11 01:38:50', NULL),
(39, 'Science', 38, NULL, NULL, '2019-08-11 01:38:58', NULL),
(40, 'Geography', 37, NULL, NULL, '2019-08-11 01:39:05', NULL),
(81, 'Business', 0, NULL, NULL, '2019-08-14 01:49:21', NULL),
(82, 'Cookbooks ', 0, NULL, NULL, '2019-08-14 01:49:32', NULL),
(83, 'Health & Fitness', 0, NULL, NULL, '2019-08-14 01:49:43', NULL),
(84, 'History', 0, NULL, NULL, '2019-08-14 01:49:55', NULL),
(85, 'Mystery', 0, NULL, NULL, '2019-08-14 01:50:04', NULL),
(86, 'Inspiration', 0, NULL, NULL, '2019-08-14 01:50:30', NULL),
(87, 'Romance', 0, NULL, NULL, '2019-08-14 01:50:38', NULL),
(88, 'Fiction/Fantasy', 0, NULL, NULL, '2019-08-14 01:50:47', NULL),
(89, 'Self-Improvement', 0, NULL, NULL, '2019-08-14 01:50:59', NULL),
(90, 'Humor Books', 0, NULL, NULL, '2019-08-14 01:51:06', NULL),
(91, 'Harry Potter', 0, NULL, NULL, '2019-08-14 01:51:16', NULL),
(92, 'Land of Stories', 0, NULL, NULL, '2019-08-14 01:51:24', NULL),
(93, 'Kids\' Music', 0, NULL, NULL, '2019-08-14 01:51:39', NULL),
(94, 'Toys & Games', 0, NULL, NULL, '2019-08-14 01:51:54', NULL),
(95, 'Hoodies', 0, NULL, NULL, '2019-08-14 01:52:12', '2019-08-14 01:52:41');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `soft_delete` int(1) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `comment`, `status`, `soft_delete`, `date`, `created_at`, `updated_at`) VALUES
(3, 'Srabanti Paul ', 'srabantipaul68@gmail.com', 'GIRL', 'very good', 1, 1, '2019-08-10 22:25:00', '2019-08-10 04:18:39', '2019-08-10 04:35:49'),
(6, 'Biggang', 'asdasd@gmail.com', 'asasd', 'asdasd', 0, 0, '2019-08-10 22:25:00', '2019-08-18 07:43:25', NULL),
(7, 'Biggang', 'asdasd@gmail.com', 'asasd', 'asdasd', 0, 0, '2018-06-17 00:00:00', '2019-08-18 08:11:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

DROP TABLE IF EXISTS `labels`;
CREATE TABLE IF NOT EXISTS `labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `title`, `picture`) VALUES
(4, 'label-1', '1.jpg'),
(5, 'label-2', '2.jpg'),
(6, 'label-3', '3.png'),
(7, 'label-4', '4.jpg'),
(8, 'label-5', '5.png'),
(9, 'label-6', '6.jpg'),
(11, 'label-7', '7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `map_product_tag`
--

DROP TABLE IF EXISTS `map_product_tag`;
CREATE TABLE IF NOT EXISTS `map_product_tag` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `purchase_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `product_id`, `qty`, `user_id`, `purchase_date`) VALUES
(7, 12, 16, 2, 36, '2019-08-16 10:08:01'),
(8, 12, 7, 1, 36, '2019-08-16 10:08:01'),
(9, 12, 13, 1, 36, '2019-08-16 10:08:01'),
(10, 12, 10, 1, 36, '2019-08-16 10:08:01'),
(11, 12, 12, 1, 36, '2019-08-16 10:08:01'),
(18, 13, 14, 2, 36, '2019-08-16 05:06:51'),
(19, 13, 15, 1, 36, '2019-08-16 05:06:51'),
(20, 13, 10, 1, 36, '2019-08-16 05:06:51'),
(21, 14, 13, 3, 36, '2019-08-16 05:08:00'),
(22, 14, 17, 1, 36, '2019-08-16 05:08:00'),
(23, 14, 14, 1, 36, '2019-08-16 05:08:00'),
(24, 14, 10, 1, 36, '2019-08-16 05:08:00'),
(26, 15, 16, 1, 36, '2019-08-16 05:12:03'),
(27, 15, 10, 1, 36, '2019-08-16 05:12:03'),
(28, 15, 12, 1, 36, '2019-08-16 05:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `description`, `link`) VALUES
(1, 'index', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Codepen'),
(3, 'cart', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Codepen'),
(4, 'banner', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Codepen');

-- --------------------------------------------------------

--
-- Table structure for table `popular_tags`
--

DROP TABLE IF EXISTS `popular_tags`;
CREATE TABLE IF NOT EXISTS `popular_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(4) DEFAULT NULL,
  `is_draft` int(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `popular_tags`
--

INSERT INTO `popular_tags` (`id`, `name`, `link`, `is_active`, `soft_delete`, `is_draft`, `created_at`, `updated_at`) VALUES
(3, 'Natoque penatibus', 'ttps://www.Natoque penatibus.comm', 1, 1, 1, '2019-08-08 05:01:37', '2019-08-18 07:35:41'),
(5, 'Biggang', 'ttps://www.Natoque penatibus.comm', 1, 1, 1, '2019-08-18 07:35:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `label_id` int(11) DEFAULT NULL,
  `total_sales` int(11) DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `is_new` tinyint(1) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `mrp` float DEFAULT NULL,
  `special_price` float DEFAULT NULL,
  `soft_delete` tinyint(1) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `picture`, `short_description`, `description`, `brand_id`, `label_id`, `total_sales`, `product_type`, `is_new`, `cost`, `mrp`, `special_price`, `soft_delete`, `is_draft`, `is_active`, `created_at`, `modified_at`) VALUES
(16, 'test_ Book', '2443376684.jpg', 'ckansclaksjdcnlkasjdc', 'naskldjclaksjdaklsjdbvlkajbvkljfbvakljbfvlkadbjfvlakjdfbv', 22, 2, 22, '89', 1, 12, 12, 12, NULL, NULL, NULL, '2019-07-30 09:26:01', '2019-08-15 04:38:57'),
(10, 'Daily Art Management', '4135851003.jpg', 'bvvbvvb', 'ass', 1, 1, 10, '92', 1, 34, 12, 32, NULL, NULL, NULL, '2019-07-28 11:27:52', '2019-08-15 04:39:32'),
(12, 'The Women In the Window', '3842888698.jpg', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 1, 1, 12, '94', 1, 56, 20.21, 46, NULL, NULL, NULL, '2019-07-28 01:18:40', '2019-08-15 04:39:25'),
(7, 'asaas', '2308578382.jpg', 'as', 'asas', 1, 1, 12, '91', 1, 123, 1, 0.123, NULL, NULL, NULL, '2019-07-28 10:18:55', '2019-08-15 04:39:41'),
(13, 'Bad Blood', '757458889.jpg', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 1, 1, 33, '95', 1, 88, 77, 66, NULL, NULL, NULL, '2019-07-28 01:19:31', '2019-08-15 04:39:18'),
(14, 'The Kiss Quotient', '3993581688.jpg', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 1, 1, 77, '93', 1, 66, 78, 89, NULL, NULL, NULL, '2019-07-28 01:20:11', '2019-08-15 04:39:13'),
(15, 'The Book Of Awesome', '2178474356.jpg', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 'orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since', 1, 1, 77, '93', 1, 66, 78, 89, NULL, NULL, NULL, '2019-07-28 01:20:47', '2019-08-15 04:39:05'),
(17, 'test_ Book', '1345071084.jpg', 'ckansclaksjdcnlkasjdc', 'naskldjclaksjdaklsjdbvlkajbvkljfbvakljbfvlkadbjfvlakjdfbv', 22, 2, 22, '95', 1, 12, 12, 12, NULL, NULL, NULL, '2019-07-30 09:26:13', '2019-08-15 04:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `sponsers`
--

DROP TABLE IF EXISTS `sponsers`;
CREATE TABLE IF NOT EXISTS `sponsers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsers`
--

INSERT INTO `sponsers` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `created_at`, `modified_at`) VALUES
(1, 'Allstate Foundation', '1.png', 'alsf.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(2, 'AS University', '2.png', 'as.edu.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(3, 'Oprah\'s Angle Network', '3.jpg', 'oprah.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(4, 'BS', '4.jpg', 'bs.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(5, 'ALA', '5.png', 'ala.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00'),
(6, 'LULU', '6.jpg', 'lulu.com', 'Ready to help ', 'html', 1, 1, 1, '2019-08-14 00:00:00', '2019-08-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `is_subscribed` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `is_subscribed`, `created_at`, `updated_at`, `reason`) VALUES
(2, 'sudiptapaul12@gmail.com', 12, '2019-08-10 06:16:55', '2019-08-10 06:17:29', 'boys '),
(3, 'srabantipaul68@gmail.com', 50, '2019-08-10 06:17:15', NULL, 'Girl ');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created_at`, `updated_at`) VALUES
(6, ' Srabanti Paul.', '2019-08-08 09:19:41', '2019-08-08 09:20:28'),
(7, ' aseer Ishraqul.', '2019-08-08 09:20:18', '2019-08-08 09:20:24'),
(8, 'gffxf', '2019-08-18 08:13:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `PASS` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `PASS`, `email`) VALUES
(24, 'test_again', '123', 'Asd@gmail.com'),
(34, 'as12', '123', 'Asd@gmail.com'),
(35, 'test', '1234', 'asd@gmasd.ocm'),
(36, 'test1', '123', 'asd@gmasd.ocm');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
