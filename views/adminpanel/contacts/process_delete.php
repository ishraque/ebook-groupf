<?php
include_once("../../../vendor/autoload.php");

use App\Contacts;

$contacts = new Contacts();
$id = $_GET['id'];
$table = "contacts";
$r = $contacts->deleteData($id, $table);

if($r)
    header("Location:index.php");
?>