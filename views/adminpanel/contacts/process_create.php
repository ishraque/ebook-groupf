<?php

include_once("../../../vendor/autoload.php");

use App\Contacts;

$contact = new Contacts();

if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $comment = $_POST['comment'];
    $status = $_POST['status'];
    $soft_delete = $_POST['soft_delete'];
    $date= $_POST['date'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "contacts";
    $r = $contact->insertData($name,$email,$subject,$comment,$status,$soft_delete,$date,$created_at, $table);

    if($r){
        header("Location:index.php");
    }
    else
        echo "Connection error";
}
