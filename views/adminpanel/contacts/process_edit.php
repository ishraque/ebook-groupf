<?php
include_once("../../../vendor/autoload.php");
use App\Contacts;
$contacts = new Contacts();
extract($_POST);
$updated_at = date("Y-m-d h:i:s", time()) ;
$table = "contacts";
$r = $contacts->update($id, $name, $email, $subject, $comment, $status, $soft_delete, $date, $updated_at, $table );

if($r)
    header("Location:index.php");

?>

