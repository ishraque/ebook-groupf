<?php
include_once("../../../vendor/autoload.php");
use App\Contacts;
$id = $_GET['id'];
$table = "contacts";
$contacts = new Contacts();
$data = $contacts->getById($id, $table);
extract($data);
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Edit Information!</title>
</head>
<body>
<h1>Edit Cart</h1>

<form method="post" action="process_edit.php">
    <input type="hidden" value="<?php echo $id; ?>" name="id">
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input name="name" value="<?php echo $name; ?>" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter name">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input name="email" value="<?php echo $email; ?>" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Subject</label>
        <input name="subject" value="<?php echo $subject; ?>" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter subject">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Comment</label>
        <input name="comment" value="<?php echo $comment; ?>" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter comment">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Status</label>
        <input name="status" value="<?php echo $status; ?>" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter status">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Soft_delete</label>
        <input name="soft_delete" value="<?php echo $soft_delete; ?>" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter soft_delete">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Date</label>
        <input name="date" value="<?php echo $date; ?>" type="datetime-local" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter date">

    </div>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>