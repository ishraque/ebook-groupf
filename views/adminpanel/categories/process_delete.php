<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/12/2019
 * Time: 2:33 AM
 */

session_start();
include_once("../../../vendor/autoload.php");
use App\Categories;

$obj = new Categories();

$r = $obj->deleteData($_GET['id'], $obj->table);

if($r)
{
    $_SESSION['deleteCat_success'] = 1;
    header("Location: ../categories.php");
}
else
    echo "Error";
