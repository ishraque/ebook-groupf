<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/12/2019
 * Time: 12:30 AM
 */

include_once("../../../vendor/autoload.php");
use App\Categories;

$obj = new Categories();

$parents = $obj->showData($obj->table);

?>


<option selected value="0">Parent...</option>


<?php
    foreach ($parents as $p){
    ?>
    <option value="<?php echo $p['id'] ?>"><?php echo $p['name']?></option>

    <?php
}
?>