<?php
session_start();
if(isset($_SESSION['category'])){

    ?>

    <div class="alert alert-success category-add-success" role="alert">
        <h4 class="alert-heading">New Category Added Successfully !</h4>

    </div>
<?php
    unset($_SESSION['category']);
}

?>

