<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/12/2019
 * Time: 1:19 AM
 */

include_once("../../../vendor/autoload.php");
use App\Categories;
session_start();
$obj = new Categories();
//
//echo "<pre>";
//print_r($_POST);
//echo "</pre>";


$r = $obj->updateCategory($_POST);

if($r)
{
    $_SESSION['updatedCat_success'] = 1;
    header("Location: ../categories.php");

}
else
    echo "Error";