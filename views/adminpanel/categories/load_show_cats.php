<?php
include_once("../../../vendor/autoload.php");
use App\Categories;

            $obj = new Categories();
            $data = $obj->showData($obj->table);
            $i = 1;
             foreach ($data as $value){
                 extract($value);

                 ?>

                 <tr>
                     <th scope="row"><?php echo $i++ ?></th>
                     <td><?php echo  $name ?></td>
                     <td><?php echo $parent?></td>
                     <td><?php echo $soft_delete ?></td>
                     <td><?php echo $is_draft ?></td>
                     <td><?php echo $created_at ?></td>
                     <td><?php echo $modified_at ?></td>
                     <td>
                         <a href="categories/edit.php?id=<?php echo $id ?>"> Edit </a> |
                         <a href="categories/process_delete.php?id=<?php echo $id ?>"> Delete </a>

                     </td>
                 </tr>


                 <?php
             }
            ?>