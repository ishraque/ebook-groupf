<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/30/2019
 * Time: 3:07 AM
 */

include_once("../../../vendor/autoload.php");

use App\Products;

$obj = new Products();

if(isset($_GET['id'])){
    extract($_GET);
    $r = $obj->deleteData($id, "products");

    if($r)
        header("Location: show.php");
    else
        echo "Error";
}