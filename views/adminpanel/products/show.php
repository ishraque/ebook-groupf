<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/29/2019
 * Time: 11:19 PM
 */

include_once("../../../vendor/autoload.php");

use App\Products;

$obj = new Products();


$data = $obj->showData("products");

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>All Products</title>
</head>
<body>
<h1 align="center"><a href="../index.php">Home</a> </h1>
<br>
<h3 align="center">All Products </h3>
<br>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#SL</th>
        <th scope="col">Title</th>
        <th scope="col">Picture</th>
        <th scope="col">short_description</th>
        <th scope="col">description</th>
        <th scope="col">brand_id</th>
        <th scope="col">label_id</th>
        <th scope="col">total_sales</th>
        <th scope="col">product_type</th>
        <th scope="col">is_new</th>
        <th scope="col">cost</th>
        <th scope="col">mrp</th>
        <th scope="col">special_price</th>
        <th scope="col">created_at</th>
        <th scope="col">modified_at</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>

    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <?php
    $obj = new Products();


    $data = $obj->showData("products");

    //  echo "<pre>";
    //  print_r($data);
    //echo "</pre>";
    $i = 1;
    foreach ($data as $value) {
        extract($value);
        ?>

        <tr>
            <th scope="row">#<?php echo $i++; ?></th>
            <td><?php echo $title; ?></td>
            <td><img width="100%" src="uploads/<?php echo $picture; ?>"></td>
            <td><?php echo $short_description; ?></td>
            <td><?php echo $description; ?></td>
            <td><?php echo $brand_id; ?></td>
            <td><?php echo $label_id; ?></td>
            <td><?php echo $total_sales; ?></td>
            <td><?php echo $product_type; ?></td>
            <td><?php echo $is_new; ?></td>
            <td><?php echo $cost; ?></td>
            <td><?php echo $mrp; ?></td>
            <td><?php echo $special_price; ?></td>
            <td><?php echo $created_at; ?></td>
            <td><?php echo $modified_at; ?></td>
            <td><a href="edit.php?id=<?php echo $id; ?>">Edit </a>
                <a onclick="confirm('Confirm Deletion ?')" href="process_delete.php?id=<?php echo $id; ?>">Delete</a>
            </td>
        </tr>


        <?php
    }
    ?>


    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->



    </tbody>
</table>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
