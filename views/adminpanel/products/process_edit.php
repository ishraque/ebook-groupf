<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 7/30/2019
 * Time: 2:56 AM
 */

include_once("../../../vendor/autoload.php");

use App\Products;

$obj = new Products();

if(isset($_POST['upload']))
{
//       echo "<pre>";
//   print_r($_POST);
//   echo "</pre>";

    $r = $obj->updateData($_POST);
    if($r)
        header("Location: show.php");
    else
        echo "Error";
}