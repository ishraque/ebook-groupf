<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Edit Product</title>

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

<div class="container">
    <div class="card card-register mx-auto mt-5">
        <div class="card-header">Edit a Product</div>
        <div class="card-body">
            <?php
            include_once("../../../vendor/autoload.php");
            use App\Products;
            use App\Categories;
            $obj = new Products();
            $data = $obj->getById($_GET['id'], 'products');
            extract($data);
            ?>
            <form action="process_edit.php" method="post" enctype="multipart/form-data">

             <input type="hidden" value="<?php echo $_GET['id']?>" name="id">

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $title?>" name="title" type="text" id="title" class="form-control" placeholder="title" required="required" >
                                <label for="title">title</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $short_description?>" name="short_description" type="text" id="short_description" class="form-control" placeholder="short_description" required="required">
                                <label for="short_description">short_description</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $description?>" name="description" type="text" id="description" class="form-control" placeholder="description" required="required" >
                                <label for="description">description</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $brand_id?>" name="brand_id" type="number" id="brand_id" class="form-control" placeholder="brand_id" required="required">
                                <label for="brand_id">brand_id</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $label_id?>" name="label_id" type="number" id="label_id" class="form-control" placeholder="label_id" required="required" >
                                <label for="label_id">label_id</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $total_sales?>" name="total_sales" type="number" id="total_sales" class="form-control" placeholder="total_sales" required="required">
                                <label for="total_sales">total_sales</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">


                                <div class="input-group mb-3">
                                    <select class="custom-select" id="parent" name="product_type">
                                        <option selected value="0">Parent...</option>

                                        <?php
                                        $cat = new Categories();
                                        $parents = $cat->showData($cat->table);


                                        foreach ($parents as $pp){
                                            if($pp['id'] == $product_type) {


                                                ?>
                                                <option <?php echo "selected" ?>
                                                        value="<?php echo $pp['id'] ?>"><?php echo $pp['name'] ?></option>

                                                <?php
                                            }else{
                                                ?>
                                                <option value="<?php echo $pp['id']; ?>"><?php echo $pp['name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>


                                    </select>

                                </div>
<!--                                <input value="--><?php //echo $product_type?><!--" name="product_type" type="text" id="product_type" class="form-control" placeholder="product_type" required="required" >-->
<!--                                <label for="product_type">product_type</label>-->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php  echo $is_new?>" name="is_new" type="number" id="is_new" class="form-control" placeholder="is_new" required="required">
                                <label for="is_new">is_new</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $cost?>" name="cost" type="number" id="cost" step="0.00001" class="form-control" placeholder="cost" required="required" >
                                <label for="cost">cost</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $mrp?>" name="mrp" type="number" id="mrp" step="0.00001" class="form-control" placeholder="mrp" required="required">
                                <label for="mrp">mrp</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <input value="<?php echo $special_price?>" name="special_price" type="number" step="0.00001" id="special_price" class="form-control" placeholder="special_price" required="required" >
                                <label for="special_price">special_price</label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-label-group">
                    <input name="upload" type="submit"  class="form-control" value="Submit" class="btn btn-primary">

                </div>


            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="login.html">Login Page</a>
                <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>
