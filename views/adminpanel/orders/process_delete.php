<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/16/2019
 * Time: 10:52 PM
 */

include_once("../../../vendor/autoload.php");
session_start();
use App\Billing;
use App\Orders;

$obj_orders = new Orders();
$obj_billing = new Billing();

if(isset($_GET['order_id'])){
    $order_id = $_GET['order_id'];
    $r = $obj_billing->deleteBillByOrderId($order_id);
    $obj_orders->deleteOrder($order_id);
    $_SESSION['deleteOrder_success'] = 1;
    header("Location: ../orders.php");
}
