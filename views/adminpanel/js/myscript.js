$(document).ready(function(){
    $("#category").keypress(function (e) {

        // 13 is ASCII of ENTER Key
        if(e.keyCode == 13){
            var name = $("#category").val();
            var parent = $("#parent").val();

            $.ajax({
                url:"categories/insert.php",
                method:"post",
                data:{ name:name, parent:parent },
                success:function(data)
                {
                    // console.log(category+"--"+parent);
                    // $('#result').html(data);
                    // console.log(data);

                    $("#taskMessage").css("display", "initial").load("categories/load_add_cat_success.php").fadeOut(5000);
                    $("#loadCategories").load("categories/load_show_cats.php");
                    $("#countCategories").load("categories/count_categories.php");
                    $("#parent").load("categories/load_parents.php");
                    // $("#cartAmountDisappear").css("display", "initial").load("cart/load_cart_amount.php").fadeOut(2000);
                    //
                    // $("#cartAmount").fadeIn("normal").load("cart/load_cart_amount.php");
                    //
                    // $("#loadMiniCart").load("cart/load_show_user_cart.php");

                }
            });
        }



    });



    $("#updatedCat_success").fadeOut(4000);
    $("#deleteCat_success").fadeOut(4000);
    $("#custom_msg").fadeOut(3000);

});
