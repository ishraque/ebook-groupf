<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Custom fonts-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
</head>

<body id="page-top">

<?php
include_once("inc/nav.php");
?>

<div id="wrapper">

    <!-- Sidebar -->

    <?php
    include_once("inc/sidebar.php");
    ?>


    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Overview</li>
            </ol>

            <!-- Icon Cards-->
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-comments"></i>
                            </div>
                            <div id="countOrders" class="mr-5 h5">


                                <?php
                                include_once("../../vendor/autoload.php");
                                use App\Orders;
                                use App\Categories;
                                use App\Cart;
                                $obj_orders = new Orders();
                                $total_orders = $obj_orders->countOrders();
                                echo $total_orders." Orders!";
                                ?>




                            </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="orders.php">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                                              <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-warning o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-list"></i>
                            </div>
                            <div class="mr-5">

                                <?php
                                $obj = new Categories();
                                $total_categories = $obj->countCategories();
                                echo $total_categories." Categories!";
                                ?>



                            </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="categories.php">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-shopping-cart"></i>
                            </div>
                            <div class="mr-5">


                                <?php
                                $obj_carts = new Cart();
                                $total_items = $obj_carts->countRows($obj_carts->table);
                                echo $total_items." Cart Items!";
                                ?>


                            </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="carts.php">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
                        </a>
                    </div>
                </div>





            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>


        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">SL</th>
                <th scope="col">User ID</th>
                <th scope="col">Product Title</th>
                <th scope="col">Image</th>
                <th scope="col">Quantity</th>
                <th scope="col">Unite Price</th>
                <th scope="col">Total Price</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
        <?php
        $data = $obj_carts->showData($obj_carts->table);
        $i = 1;
        foreach ($data as $value){
extract($value);
            ?>
            <tr>
                <th scope="row"><?php echo $i++ ?></th>
                <td><?php echo $user_id ?></td>
                <td><?php echo $product_title ?></td>
                <td><img class="img-responsive w-25" src="products/uploads/<?php echo $picture ?>" ></td>
                <td><?php echo $qty ?></td>
                <td><?php echo $unite_price ?></td>
                <td><?php echo $total_price ?></td>
                <td>

                    <a href="cart/process_delete.php?id=<?php echo $id ?>">
                        Delete
                    </a>
                </td>
            </tr>
        <?php
        }
        ?>






            </tbody>
        </table>




    </div>

</div>
<!-- /.container-fluid -->

<!-- Sticky Footer -->
<?php
include_once("inc/footer.php");
?>



</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.php">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/myscript.js"></script>
</body>

</html>
