<ul class="sidebar navbar-nav">
    <li class="nav-item active">
        <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Action Screens:</h6>


            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Log in Screens:</h6>
            <a class="dropdown-item" href="login.php">Login</a>
            <a class="dropdown-item" href="register.php">Register</a>
            <a class="dropdown-item" href="forgot-password.php">Forgot Password</a>
        </div>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Products</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Action Screens:</h6>
            <a class="dropdown-item" href="products/show.php"> Show All Products</a>
            <a class="dropdown-item" href="products/products_insert_form.php">Upload Product</a>
            <!--<a class="dropdown-item" href="cart/views/carts.php">Carts</a>-->


        </div>
    </li>





    <li class="nav-item">
        <a class="nav-link" href="categories.php">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Categories</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="orders.php">
            <i class="fas fa-fw fa-table"></i>
            <span>Orders</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="orders.php">
            <i class="fas fa-fw fa-cart-arrow-down"></i>
            <span>Carts</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="banners/">
            <i class="fas fa-fw fa-image"></i>
            <span>Banners</span></a>


    </li>
    <li class="nav-item">
        <a class="nav-link" href="labels/">
            <i class="fas fa-fw fa-coffee"></i>
            <span>Labels</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="brands/">
            <i class="fas fa-fw fa-code-branch"></i>
            <span>Brands</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="sponsers/">
            <i class="fas fa-fw fa-people-carry"></i>
            <span>Sponsers</span></a>


    </li>
    <li class="nav-item">
        <a class="nav-link" href="pages/">
            <i class="fas fa-fw fa-pager"></i>
            <span>Pages</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="tags/">
            <i class="fas fa-fw fa-tag"></i>
            <span>Tags</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="popular_tags/">
            <i class="fas fa-fw fa-tags"></i>
            <span>Populars Tags</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="subscribers/">
            <i class="fas fa-fw fa-people-carry"></i>
            <span>Subscribers</span></a>


    </li>

    <li class="nav-item">
        <a class="nav-link" href="contacts/">
            <i class="fas fa-fw fa-mobile"></i>
            <span>Contacts</span></a>


    </li>










<!--    <li class="nav-item">-->
<!--        <a class="nav-link" href="#">-->
<!--            <i class="fas fa-fw fa-table"></i>-->
<!--            <span>#</span></a>-->
<!--    </li>-->
</ul>