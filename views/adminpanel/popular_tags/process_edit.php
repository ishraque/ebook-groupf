<?php
include_once("../../../vendor/autoload.php");

use App\Popular_tags;

$popular_tags = new Popular_tags();
extract($_POST);
$updated_at = date("Y-m-d h:i:s", time()) ;
$table = "popular_tags";
$r = $popular_tags->update($id,$name,$link,$is_active,$soft_delete,$is_draft,$updated_at,$table );

if($r)
    header("Location:index.php");
?>

