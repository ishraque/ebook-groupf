<?php

use App\Popular_tags;

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Link</th>
        <th scope="col">Is_active</th>
        <th scope="col">Soft_delete</th>
        <th scope="col">Is_draft</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>

    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <?php
    $popular_tag = new Popular_tags();
    $data = $popular_tag->showData("popular_tags");
    // echo "<pre>";
    // print_r($data);
    //  echo "</pre>";
    $i = 1;
    foreach ($data as $value) {
        extract($value);
        ?>

        <tr>
            <th scope="row"><?php echo $i++; ?></th>
            <td><?php echo $name; ?></td>
            <td><?php echo $link; ?></td>
            <td><?php echo $is_active; ?></td>
            <td><?php echo $soft_delete; ?></td>
            <td><?php echo $is_draft; ?></td>
            <td><a href="edit.php?id=<?php echo $id; ?>">Edit </a>
                <a href="process_delete.php?id=<?php echo $id; ?>">Delete</a>
            </td>
        </tr>


        <?php
    }
    ?>


    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->
    <!--   Loop -->



    </tbody>
</table>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>