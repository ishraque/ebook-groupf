<?php

include_once("../../../vendor/autoload.php");

use App\Popular_tags;

$popular_tag = new Popular_tags();

if(isset($_POST['submit']))
{
    $name = $_POST['name'];
    $link = $_POST['link'];
    $is_active = $_POST['is_active'];
    $soft_delete = $_POST['soft_delete'];
    $is_draft= $_POST['is_draft'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "popular_tags";
    $r = $popular_tag->insertData($name,$link, $is_active,$soft_delete,$is_draft,$created_at, $table);

    if($r){
        header("Location:index.php");
    }
    else
        echo "Connection error";
}
