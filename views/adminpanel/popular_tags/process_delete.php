<?php
include_once("../../../vendor/autoload.php");

use App\Popular_tags;

$popular_tags = new Popular_tags();
$id = $_GET['id'];
$table = "popular_tags";
$r = $popular_tags->deleteData($id, $table);

if($r)
    header("Location:index.php");
?>