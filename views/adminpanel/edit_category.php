<?php
include_once("../../vendor/autoload.php");
use App\Categories;
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Custom fonts-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

</head>

<body id="page-top">

<?php
include_once("inc/nav.php");
?>

<div id="wrapper">

    <!-- Sidebar -->

    <?php
    include_once("inc/sidebar.php");
    ?>


    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Categories</li>
            </ol>

            <!-- Icon Cards-->
            <div class="row">



                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-comments"></i>
                            </div>
                            <div id="countCategories" class="mr-5 h5">


                                <?php
                                $obj = new Categories();
                                $total_categories = $obj->countCategories();
                                echo $total_categories." Categories!";
                                ?>




                            </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <!--                            <span class="float-left">View Details</span>-->
                            <!--                            <span class="float-right">-->
                            <!--                  <i class="fas fa-angle-right"></i>-->
                            </span>
                        </a>
                    </div>
                </div>


                <div class="col-xl-5 col-sm-6 mb-3">

                    <div class="card text-white bg-warning o-hidden h-100">

                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-list"></i>
                            </div>


                            <?php
                            $data = $obj->getById($_GET['id'], $obj->table);
                            extract($data);
                            ?>
                          <form action="categories/process_edit.php" method="post">
                              <input type="hidden" value="<?php echo $id?>" name="id">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <div class="form-label-group">
                                          <input value="<?php echo $name?>" autofocus="autofocus" name="name" type="text" id="category" class="form-control" placeholder="category" required="required" >
<!--                                          <label for="category"></label>-->
                                      </div>

                                  </div>

                                  <div class="col-sm-6">
                                      <div class="input-group mb-3">
                                          <select class="custom-select" id="parent" name="parent">
                                              <option selected value="0">Parent...</option>

                                              <?php
                                              $parents = $obj->showData($obj->table);
                                              foreach ($parents as $p){
                                                  if($p['id'] == $parent) {


                                                      ?>
                                                      <option <?php echo "selected" ?>
                                                          value="<?php echo $p['id'] ?>"><?php echo $p['name'] ?></option>

                                                      <?php
                                                  }else{
                                                      ?>
                                                      <option value="<?php echo $p['id'] ?>"><?php echo $p['name'] ?></option>
                                              <?php
                                                  }
                                              }
                                              ?>


                                          </select>

                                      </div>
                                  </div>

                              </div>





                        </div>

                        <span class="ml-5 ">




<button name="update" type="submit" class="btn btn-dark mb-2">Update </button>



                            </span>
                        </form>


                    </div>

                </div>










            </div>
        </div>











    </div>

</div>
<!-- /.container-fluid -->

<!-- Sticky Footer -->
<?php
include_once("inc/footer.php");

?>



</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.php">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/myscript.js"></script>
</body>

</html>
