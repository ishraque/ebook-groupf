<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/8/2019
 * Time: 3:44 PM
 */
include_once("../../../vendor/autoload.php");

session_start();
use App\Cart;
$obj = new Cart();

if(isset($_GET['id']))
{
    extract($_GET);
    $r = $obj->deleteData($id, $obj->table);
    if($r)
    {
        $_SESSION['cart_item_removed'] = 1;
        header("Location: ../carts.php");
    }

    else
        echo "Error Deleting";
}