<?php

include_once("../vendor/autoload.php");

use App\Subscribers;

$subscriber = new Subscribers();

if(isset($_POST['submit']))
{
    $email = $_POST['email'];
    $is_subscribed = $_POST['is_subscribed'];
    $reason = $_POST['reason'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "subscribers";
    $r = $subscriber->insertData($email,$is_subscribed,$reason, $created_at, $table);

    if($r){
        header("Location:index.php");
    }
    else
        echo "Connection error";
}
