<?php
include_once("../vendor/autoload.php");
use App\Subscribers;
$subscribers = new Subscribers();
extract($_POST);
$updated_at = date("Y-m-d h:i:s", time()) ;
$table = "subscribers";
$r = $subscribers->update($id, $email, $is_subscribed, $reason, $updated_at, $table );

if($r)
    header("Location:index.php");

?>

