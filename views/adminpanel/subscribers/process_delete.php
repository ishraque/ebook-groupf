<?php
include_once("../vendor/autoload.php");

use App\Subscribers;

$subscribers = new Subscribers();
$id = $_GET['id'];
$table = "subscribers";
$r = $subscribers->deleteData($id, $table);

if($r)
    header("Location:index.php");
?>