
<?php
include_once("../../../vendor/autoload.php");

use App\Sponsers;

$sponsers = new \App\Sponsers();
$id = $_GET['id'];
$table = "sponsers";
if(isset($_POST['update'])) {
    extract($_POST);
    if ($sponsers->update($id,$title,$picture, $link,$promotional_message,$html_banner,$is_active,$table)){
        header("Location:index.php");
    }
}
extract($sponsers->getById($id,$table));
?>

<body>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css"/>
    <script src="../../js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>
<div class="container">
    <form action="edit.php" method="post">
        <div class="brands_adding form">
            <div class="row">


                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input type="text" name="id" value="<?php echo $id; ?>" class="form-control" id="id" required placeholder="Id"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="<?php echo $title; ?>" class="form-control" id="title" required placeholder="Title"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="picture" value="<?php echo $picture; ?>" class="form-control" id="picture" required placeholder="Picture"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" name="link" value="<?php echo $link; ?>" class="form-control" id="link" required placeholder="Link"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="promotional_message">Promotional Message</label>
                        <input type="text" name="promotional_message" value="<?php echo $promotional_message; ?>"class="form-control" id="promotional_message" required
                               placeholder="Promotional Message"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="html_banner">HTML Banner</label>
                        <input type="text" name="html_banner" value="<?php echo $html_banner; ?>"class="form-control" id="html_banner" required
                               placeholder="HTML Banner"><br><br>

                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="is_active">Is Active</label>
                        <input type="text" name="is_active" value="<?php echo $is_active; ?>"class="form-control" id="is_active" required
                               placeholder="Is Active"><br><br>

                    </div>
                </div>


            </div>
        </div>
        <input type="submit" name="update" required placeholder="Update Data">
    </form>
</div>
</body>
</html>
