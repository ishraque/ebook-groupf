
<?php
include_once("../../../vendor/autoload.php");
use App\Pages;
$pages = new \App\Pages();
$id = $_GET['id'];
$table = "pages";
if(isset($_POST['update'])) {
    extract($_POST);
    if ($pages->update($id, $title, $description,$link,$table)){
        header("Location:index.php");
    }
}
extract($pages->getById($id,$table));
?>

<body>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>
<div class="container">
    <form action="edit.php" method="post">
        <div class="label_editing_form">
            <div class="row">


                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input type="text" name="id" value="<?php echo $id; ?>" class="form-control" id="id" required placeholder="Id"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="<?php echo $title; ?>" class="form-control" id="title" required placeholder="Title"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" name="description" value="<?php echo $description; ?>" class="form-control" id="description" required placeholder="description"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" name="link" value="<?php echo $link; ?>" class="form-control" id="link" required placeholder="link"><br><br>

                    </div>
                </div>
            </div>
            <input type="submit" name="update" required placeholder="Update Data">
    </form>
</div>
</body>
</html>
