<?php
include_once("../../../vendor/autoload.php");
use App\Labels;
$table = "pages";
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
    <title>Pages View for Admin</title>
    <style>
        .active{
            color: green;
        }
        .inactive{
            color: red;
        }
    </style>
</head>
<body>
<a href="../index.html">Get back to Home</a>
<main>
    <div class="container pages_headline">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Page View</h1>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="offset-md-6">
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="create.php" style="color: black">Add New label</a>
                </button>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-sm table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">link</th>
                        </tr>
                        </thead>
                        <tbody>



                        <?php

                        $pages = new \App\Labels();
                        $data  = $pages->showData("pages");
                        // echo "<pre>";
                        // print_r($data);
                        //echo "</pre>";
                        $i = 1;
                        foreach ($data as $value) {
                            extract($value);
                            ?>

                            <tr>

                                <td><?php echo $value['id'] ?></td>
                                <td><?php echo $value['title'] ?></td>
                                <td><?php echo $value['description'] ?></td>
                                <td><?php echo $value['link'] ?></td>
                                <td><a href="edit.php?id=<?php echo $value['id']?>">Edit</a>|
                                    <a href="delete.php?id=<?php echo $value['id']?>"> Delete</a>|
                                </td>
                            </tr>
                            <?php
                        }
                        ?>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</main>





<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="../../../lib/js/bootstrap.min.js"></script>
</body>
</html>