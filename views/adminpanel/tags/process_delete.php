<?php

include_once("../../../vendor/autoload.php");

use App\Tags;

$tags = new Tags();
$id = $_GET['id'];
$table = "tags";
$r = $tags->deleteData($id, $table);

if($r)
    header("Location:index.php");
?>