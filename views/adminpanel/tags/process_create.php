<?php


include_once("../../../vendor/autoload.php");

use App\Tags;

$tag = new Tags();

if(isset($_POST['submit']))
{
    $title = $_POST['title'];
    $created_at = date("Y-m-d h:i:s", time()) ;
//$table = $_POST['table'];
    $table = "tags";
    $r = $tag->insertData($title, $created_at, $table);

    if($r){
        header("Location:index.php");
    }
    else
        echo "Connection error";
}