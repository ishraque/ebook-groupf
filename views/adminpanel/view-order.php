<?php
include_once("../../vendor/autoload.php");
use App\Orders;
use App\Billing;

if(isset($_GET['order_id'])){
    $order_id = $_GET['order_id'];
}
//else
//    header("Location: orders.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Dashboard</title>

    <!-- Custom fonts-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

</head>

<body id="page-top">

<?php
include_once("inc/nav.php");
?>

<div id="wrapper">

    <!-- Sidebar -->

    <?php
    include_once("inc/sidebar.php");
    ?>


    <div id="content-wrapper">

        <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item ">Orders</li>
                <li class="breadcrumb-item active">Order No <?php echo "#".crc32($order_id) ?></li>
            </ol>

            <div class="row">
                <div class="col-lg-8 m-auto">
                    <h3 class="text-center">Bill No <?php echo "#".crc32($order_id) ?></h3>
                    <?php


                    $obj_orders = new Orders();
                    $obj_billing = new Billing();
                    $bill = $obj_billing->getBillByOrderId($order_id);
                    $orders = $obj_orders->showOrders($order_id);
                    $shipping = 100;
                    extract($bill);

                    ?>

                    <table class="table table-striped">

                        <tbody>




                        <tr>

                            <td> <span class="h6 mr-3 ">Order No:</span> <?php echo "#".crc32($order_id) ?></td>
                            <td><span class="h6 mr-3">Date:</span><?php echo $purchase_date ?> </td>

                        </tr>
                        <tr>

                            <td colspan="2"><span class="h6 mr-3">Customer Name: </span>   <?php echo $firstName." ".$lastName ?></td>


                        </tr>



                        <tr>

                            <td colspan="2"><span class="h6 mr-3">Address: </span>   <?php echo $address ?></td>


                        </tr>

                        <tr>

                            <td><span class="h6 mr-3">Email: </span>   <?php echo $email ?></td>
                            <td><span class="h6 mr-3">Company Name: </span>   <?php echo $companyName ?></td>


                        </tr>
                        <tr>

                            <td><span class="h6 mr-3">Phone:  </span>   <?php echo $phone ?></td>
                            <td><span class="h6 mr-3">Country:  </span> <?php echo $country ?> </td>

                        </tr>

                        <tr>

                            <td><span class="h6 mr-3">District:  </span>   <?php echo $district ?></td>
                            <td><span class="h6 mr-3">Postal Code/ Zip:  </span>   <?php echo $zip ?></td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">SL</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Extended Price</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $i = 1;
                        $total = 0;
                        foreach ($orders as $order){
                            extract($order);
                            ?>




                            <tr>
                                <th scope="row"><?php echo $i++ ?></th>
                                <td><?php echo $title ?></td>
                                <td><?php echo "$".$cost ?></td>
                                <td><?php echo $qty ?></td>
                                <td><?php echo "$".$cost*$qty; ?></td>

                            </tr>

                            <?php
                            $total+= $cost*$qty;
                        }
                        ?>
                        <tr>
                            <th colspan="3" scope="row"></th>


                            <th>Shipping</th>
                            <td><?php echo "$".$shipping ?></td>
                        </tr>
                        <tr>
                            <th colspan="3" scope="row"></th>


                            <th>Total</th>
                            <td><?php echo "$".($total+$shipping) ?></td>
                        </tr>
                        </tbody>
                    </table>



                </div>
            </div>



        </div>











    </div>

</div>
<!-- /.container-fluid -->

<!-- Sticky Footer -->
<?php
include_once("inc/footer.php");

?>



</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.php">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>
<script src="js/myscript.js"></script>
</body>

</html>
