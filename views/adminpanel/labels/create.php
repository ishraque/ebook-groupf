<?php
include_once("../../../vendor/autoload.php");
use App\Labels;
$labels = new \App\Labels();
$table = "labels";
if(isset($_POST['insert'])){
    extract($_POST);

    if($labels->insertData($title, $picture, $table)
    ){
        header("Location:index.php");
    }

}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
    <title>Label View for Admin</title>
</head>
<body>



<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>
<div class="container">
    <form action="create.php" method="post">
        <div class="label_adding form">
            <div class="row">

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" required placeholder="Title"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="picture" class="form-control" id="picture" required placeholder="Picture"><br><br>

                    </div>
                </div>


</div>
<input type="submit" name="insert" value="Insert Data">
</form>
</div>
</body>
</html>

