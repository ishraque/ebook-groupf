<?php
include_once("../../../vendor/autoload.php");
use App\Brands;
$table = "brands";
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
    <title>Brands View for Admin</title>
    <style>
        .active{
            color: green;
        }
        .inactive{
            color: red;
        }
    </style>
</head>
<body>
<a href="../index.html">Get back to Home</a>
<main>
    <div class="container pages_headline">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Brands View</h1>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="offset-md-6">
                <button type="button" class="btn btn-sm btn-outline-secondary">
                    <span data-feather="calendar"></span>
                    <a href="create.php" style="color: black">Add New Brand</a>
                </button>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-sm table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Title</th>
                            <th scope="col">Link</th>
                            <th scope="col">ACTION</th>
                            <th scope="col">Is_active</th>


                        </tr>
                        </thead>
                        <tbody>



                        <?php

                        $brands = new \App\Brands();
                        $data  = $brands->showData("brands");
                        // echo "<pre>";
                        // print_r($data);
                        //echo "</pre>";
                        $i = 1;
                        foreach ($data as $value) {
                            extract($value);
                            ?>

                            <tr>

                                <td><?php echo $value['id'] ?></td>
                                <td><?php echo $value['title'] ?></td>
                                <td><?php echo $value['link'] ?></td>
                                <td><a href="edit.php?id=<?php echo $value['id']?>">Edit</a>|
                                    <a href="delete.php?id=<?php echo $value['id']?>"> Delete</a>
                                </td>
                                <td>
                                    <?php
                                    if($value['is_active'] > 0) {
                                        ?>
                                        <span class="active">Active</span>
                                        <a href="inactivate.php?id=<?= $value['id']?>" class="btn-success">Deactivate</a>
                                        <?php
                                    }else{
                                        ?>
                                        <span class="inactive">In Active</span>
                                        <a href="activate.php?id=<?= $value['id']?>" class="btn-success">Activate</a>
                                        <?php
                                    }
                                    ?>
                                </td>

                            </tr>
                            <?php
                        }
                        ?>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</main>





<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="../../../lib/js/bootstrap.min.js"></script>
</body>
</html>