
<?php
include_once("../../../vendor/autoload.php");

use App\Brands;

$brands = new \App\Brands();
$id = $_GET['id'];
$table = "brands";
if(isset($_POST['update'])) {
    extract($_POST);
    if ($brands->update($id, $title,$link,$is_draft,$is_active,$soft_delete,$created_at,$modified_at,$table)){
        header("Location:index.php");
    }
}
extract($brands->getById($id,$table));
?>

<body>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css"/>
    <script src="../../js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>
<div class="container">
    <form action="edit.php" method="post">
        <div class="brands_adding form">
            <div class="row">


                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input type="text" name="id" value="<?php echo $id; ?>" class="form-control" id="id" required placeholder="Id"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" value="<?php echo $title; ?>" class="form-control" id="title" required placeholder="Title"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" name="link" value="<?php echo $link; ?>" class="form-control" id="link" required placeholder="Link"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="is_draft">Is Draft</label>
                        <input type="text" name="is_draft" class="form-control" id="Is Draft" required
                               placeholder="is_draft"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="is_active">Is Active</label>
                        <input type="text" name="is_active" class="form-control" id="is_active" required
                               placeholder="Is Active"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="soft_delete">Soft Delete</label>
                        <input type="text" name="soft_delete" class="form-control" id="soft_delete" required
                               placeholder="Soft Delete"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="created_at">Created At</label>
                        <input type="date" name="created_at" class="form-control" id="created_at" required
                               placeholder="Created At"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="modified_at">Modified At</label>
                        <input type="date" name="modified_at" class="form-control" id="modified_at" required
                               placeholder="Modified At"><br><br>

                    </div>
                </div>

            </div>
</div>
<input type="submit" name="update" required placeholder="Update Data">
</form>
</div>
</body>
</html>
