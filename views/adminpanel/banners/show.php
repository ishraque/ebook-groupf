
<?php
include_once("../../../vendor/autoload.php");
use App\Brands;
$banners = new \App\Banners();
$id = $_GET['id'];
$table = "banners";
$data  = $banners->getById($id, $table);


?>


<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
    <title>Banner View for Admin</title>
    <style>
        .active{
            color: green;
        }
        .inactive{
            color: red;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

    <div class="row">
        <div class="col-md-12 ">
            <section>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm  ">
                            <div class="banner">
                                <a href="#" class="img-prod">
                                    <div class="img"><img src="<?= "../bannerpic/"; ?><?php echo $data['picture'] ?>"
                                                          width="750px" height="350px"></div>

                                </a>
                                <div class="text ">
                                    &nbsp;
                                    <h3><?php echo $data['title'] ?></h3><br>
                                    <div class="d-flex">

                                        <p class="col-lg-4"><?php echo $data['link'] ?>&nbsp;
                                        </p><br>
                                        <div class="col-lg-6">
                                            <?php echo $data['promotional_message'] ?>
                                            <?php echo $data['html_banner'] ?>
                                        </div>

                                        &nbsp;

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>

</main>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="../../../lib/js/bootstrap.min.js"></script>
</body>
</html>










