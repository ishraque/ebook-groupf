<?php
include_once("../../../vendor/autoload.php");
use App\Brands;
$banners = new \App\Banners();
$table = "banners";
if(isset($_POST['insert'])){
    extract($_POST);

    if($banners->insertData( $title,$picture, $link,$promotional_message,$html_banner,$is_active, $is_draft, $soft_delete,$max_display,$created_at,$modified_at,$table)
    ){
        header("Location:index.php");
    }

}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" href="../../css/bootstrap.min.css" />
    <script src="../../js/bootstrap.min.js"></script>
</head>
<body>



<div class="container">
    <div class="row">
        <div class="offset-md-6">
            <button type="button" class="btn btn-sm btn-outline-secondary">
                <span data-feather="calendar"></span>
                <a href="index.php" style="color: black">Home</a>
            </button>
        </div>
    </div>
</div>
<br/><br/>
<div class="container">
    <form action="create.php" method="post">
        <div class="banner_adding form">
            <div class="row">

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="title" required placeholder="Title"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <input type="file" name="picture" class="form-control" id="picture" required placeholder="Picture"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="link">Link</label>
                        <input type="text" name="link" class="form-control" id="link" required placeholder="link"><br><br>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="promotional_message">Promotional Message</label>
                        <input type="text" name="promotional_message" class="form-control" id="promotional_message" required placeholder="Promotional Message"><br><br>

                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="html_banner">HTML Banner</label>
                    <input type="text" name="html_banner" class="form-control" id="html_banner" required placeholder="HTML Banner"><br><br>

                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="is_active">Is Active</label>
                <input type="text" name="is_active" class="form-control" id="is_active" required placeholder="Is Active"><br><br>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="is_draft">Is Draft</label>
                <input type="text" name="is_draft" class="form-control" id="is_draft" required placeholder="Is Draft"><br><br>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="soft_delete">Soft Delete</label>
                <input type="text" name="soft_delete" class="form-control" id="soft_delete" required placeholder="Soft Delete"><br><br>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="max_display">Max Display</label>
                <input type="text" name="max_display" class="form-control" id="max_display" required placeholder="Max Display"><br><br>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="created_at">Created At</label>
                <input type="date" name="created_at" class="form-control" id="created_at" required placeholder="Created At"><br><br>

            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="modified_at">Modified At</label>
                <input type="date" name="modified_at" class="form-control" id="modified_at" required placeholder="Modified At"><br><br>

            </div>
        </div>
</div>
<input type="submit" name="insert" value="Insert Data">
</form>
</div>
</body>
</html>

