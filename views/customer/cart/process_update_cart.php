<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/8/2019
 * Time: 2:53 PM
 */

include_once("../../../vendor/autoload.php");

session_start();
use App\Cart;

$user_id = $_SESSION['user_id'];
$obj = new Cart();

if(isset($_POST['update']))
{
    extract($_POST);
 $i = 0;
    foreach ($id as $value)
    {
//        echo $value.",";
        $r = $obj->updateCart($value, $unite_price[$i], $qty[$i]);
        $i++;
    }

    if($r){
        $_SESSION['cart_updated'] = 1;
        header("Location: ../cart.php");

    }

}