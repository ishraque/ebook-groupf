<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/6/2019
 * Time: 2:36 PM
 */

include_once("../../../vendor/autoload.php");
session_start();
use App\Cart;

$user_id = $_SESSION['user_id'];

$product_id = $_POST['product_id'];

$qty = $_POST['qty'];

$obj = new Cart();

$r = $obj->addToCart($user_id, $product_id, $qty);

if($r)
    echo "Successful";
else
    echo "Error";


//echo "Hello";
