<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/7/2019
 * Time: 2:18 PM
 */

include_once("../../../vendor/autoload.php");

session_start();
use App\Cart;

$user_id = $_SESSION['user_id'];
$obj = new Cart();


    if(isset($user_id)){
        $items = $obj->showCartItems($user_id);
        $subtotal = $obj->cartSubTotal($user_id);
        $cart_items = $obj->showUserCart($user_id);
       ?>



                            <div class="minicart-content-wrapper">
                                <div class="micart__close">
                                    <span>close</span>
                                </div>
                                <div class="items-total d-flex justify-content-between">
                                    <span><span style="width:50px;height: 50px;"></span> <?php echo $items ?>  items</span>
                                    <span>Cart Subtotal</span>
                                </div>
                                <div class="total_amount text-right">
                                    <span>$<?php echo $subtotal ?></span>
                                </div>
                                <div class="mini_action checkout">
                                    <a class="checkout__btn" href="checkout.php">Go to Checkout</a>
                                </div>
                                <div class="single__items">
                                    <div class="miniproduct">

                                   <?php


                                   $i = 1;
                                   foreach ($cart_items as $cart_item){
                                       extract($cart_item);
                                       ?>


                                       <div class="item01 d-flex mt--20">
                                           <div class="thumb">
                                               <a href="product-details.php"><img src="../adminpanel/products/uploads/<?php echo $picture ?>" alt="product images"></a>
                                           </div>
                                           <div class="content">
                                               <h6><a href="product-details.php?id=<?php echo $picture ?>"><?php echo $product_title ?></a></h6>
                                               <span class="prize">$<?php echo $unite_price ?></span>
                                               <div class="product_prize d-flex justify-content-between">
                                                   <span class="qun">Qty: <?php echo $qty ?></span>
                                                   <ul class="d-flex justify-content-end">
                                                       <li><a href="#"><i class="zmdi zmdi-settings"></i></a></li>
                                                       <li><a href="#"><i class="zmdi zmdi-delete"></i></a></li>
                                                   </ul>
                                               </div>
                                           </div>
                                       </div>

                                        <?php
                                       $i++;
                                       if($i == 4)
                                           break;
                                   }
                                   ?>






<!--                                        <div class="item01 d-flex mt--20">-->
<!--                                            <div class="thumb">-->
<!--                                                <a href="product-details.php"><img src="images/product/sm-img/2.jpg" alt="product images"></a>-->
<!--                                            </div>-->
<!--                                            <div class="content">-->
<!--                                                <h6><a href="product-details.php">Compete Track Tote</a></h6>-->
<!--                                                <span class="prize">$40.00</span>-->
<!--                                                <div class="product_prize d-flex justify-content-between">-->
<!--                                                    <span class="qun">Qty: 03</span>-->
<!--                                                    <ul class="d-flex justify-content-end">-->
<!--                                                        <li><a href="#"><i class="zmdi zmdi-settings"></i></a></li>-->
<!--                                                        <li><a href="#"><i class="zmdi zmdi-delete"></i></a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                        </div>-->






                                    </div>
                                </div>
                                <div class="mini_action cart">
                                    <a class="cart__btn" href="cart.php">View and edit cart</a>
                                </div>
                            </div>

    <?php
    }




//echo "<pre>";
//print_r($data);
//echo "</pre>";


    ?>