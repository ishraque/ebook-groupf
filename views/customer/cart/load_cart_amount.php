<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/7/2019
 * Time: 11:31 AM
 */

include_once("../../../vendor/autoload.php");

session_start();
use App\Cart;

$user_id = $_SESSION['user_id'];

if(isset($user_id))
{
    $obj = new Cart();
    $sum = $obj->showCartAmount($user_id);
    if($sum != 0 )
        echo $sum;
    else
        echo 0;
}
else
    echo 0;