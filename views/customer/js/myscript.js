function addToCart(product_id, qty){
    $.ajax({
        url:"cart/add_to_cart.php",
        method:"post",
        data:{ product_id:product_id, qty:qty },
        success:function(data)
        {
            // $('#result').html(data);
            //  console.log(data);

            $("#cartAmountDisappear").css("display", "initial").load("cart/load_cart_amount.php").fadeOut(2000);

            $("#cartAmount").fadeIn("normal").load("cart/load_cart_amount.php");

            $("#loadMiniCart").load("cart/load_show_user_cart.php");

        }
    });

}



$(document).ready(function(){


    // Login Message --------->>>>>>>>
$("#custom_msg").fadeOut(3000);
// Login Message ---------<<<<<


// add to cart from product view page---START
// add to cart from product view page---START
$("#addCartQty").click(function () {
    var qty = $("#qty").val();
    var product_id = $("#addCartQty").val();

    //console.log(qty+"--"+product_id);

    addToCart( product_id, qty);

});

// add to cart from product view page---END
// add to cart from product view page---END


});