<?php
include_once("../../../vendor/autoload.php");
use App\Users;
session_start();


if(isset($_POST['login']))
{
    $obj = new Users();
    $id = $obj->login($_POST);
    if($id)
    {
        $_SESSION['user_id'] = $id;
        $_SESSION['logged_in'] = 1;
        header("Location: ../index.php");

    }

    else
        echo "Error Logging in";
}