<?php
/**
 * Created by PhpStorm.
 * User: Ishraqul Hoque
 * Date: 8/15/2019
 * Time: 11:50 AM
 */

include_once("../../../vendor/autoload.php");
use App\Users;
session_start();

session_destroy();
header("Location: ../index.php");