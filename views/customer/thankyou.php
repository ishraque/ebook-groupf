<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Thank you | ESHOP</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/icon.png">

    <!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/plugins.css">
    <link rel="stylesheet" href="style.css">

    <!-- Cusom css -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Modernizer js -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    <!-- Header -->
    <?php
    include_once("inc/header.php");
    ?>
    <!-- //Header -->
    <!-- Start Search Popup -->
    <div class="box-search-content search_active block-bg close__top">
        <form id="search_mini_form" class="minisearch" action="#">
            <div class="field__search">
                <input type="text" placeholder="Search entire store here...">
                <div class="action">
                    <a href="#"><i class="zmdi zmdi-search"></i></a>
                </div>
            </div>
        </form>
        <div class="close__wrap">
            <span>close</span>
        </div>
    </div>
    <!-- End Search Popup -->
    <!-- Start Bradcaump area -->
    <div class="ht__bradcaump__area bg-image--5">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Thank You</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="index.html">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Checkout</span>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Thank You</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bradcaump area -->

    <!-- Start Error Area -->
    <section class="page_error section-padding--lg bg--white">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 m-auto">

                    <h2 class="text-center">Thank you for your purchase!</h2>
                    <p>Your books will be delivered to you within 15-20 business days. If you didn't receive your items within the allocated period, please inform us. There are customer protection services which include Refunding, items redelivering etc and many more.<br>
                        Email: info@gmail.com <br>
                        Contact: 090-3742734, 123-12312344, 231-1231442.
                    </p>
                   <h3 class="text-center">Your Bill</h3>
                    <?php
                    if(isset($_SESSION['order_id'])){
                     $order_id = $_SESSION['order_id'];
                    }
                    else
                        header("error404.html");


                    use App\Orders;
                    use App\Billing;
//                    $order_id = 12;
                    $obj_orders = new Orders();
                    $obj_billing = new Billing();
                    $bill = $obj_billing->getBillByOrderId($order_id);
                    $orders = $obj_orders->showOrders($order_id);
                    $shipping = 100;
                   extract($bill);

                    ?>

                    <table class="table table-striped">

                        <tbody>




                        <tr>

                            <td> <span class="h6 mr-3 ">Order No:</span> <?php echo "#".crc32($order_id) ?></td>
                            <td><span class="h6 mr-3">Date:</span><?php echo $purchase_date ?> </td>

                        </tr>
                        <tr>

                            <td colspan="2"><span class="h6 mr-3">Customer Name: </span>   <?php echo $firstName." ".$lastName ?></td>


                        </tr>



                        <tr>

                            <td colspan="2"><span class="h6 mr-3">Address: </span>   <?php echo $address ?></td>


                        </tr>

                        <tr>

                            <td><span class="h6 mr-3">Email: </span>   <?php echo $email ?></td>
                            <td><span class="h6 mr-3">Company Name: </span>   <?php echo $companyName ?></td>


                        </tr>
                        <tr>

                            <td><span class="h6 mr-3">Phone:  </span>   <?php echo $phone ?></td>
                            <td><span class="h6 mr-3">Country:  </span> <?php echo $country ?> </td>

                        </tr>

                        <tr>

                            <td><span class="h6 mr-3">District:  </span>   <?php echo $district ?></td>
                            <td><span class="h6 mr-3">Postal Code/ Zip:  </span>   <?php echo $zip ?></td>

                        </tr>
                        </tbody>
                    </table>

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">SL</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Extended Price</th>
                        </tr>
                        </thead>
                        <tbody>

                <?php
                $i = 1;
                $total = 0;
                foreach ($orders as $order){
                    extract($order);
                    ?>




                        <tr>
                            <th scope="row"><?php echo $i++ ?></th>
                            <td><?php echo $title ?></td>
                            <td><?php echo "$".$cost ?></td>
                            <td><?php echo $qty ?></td>
                            <td><?php echo "$".$cost*$qty; ?></td>

                        </tr>

                <?php
                    $total+= $cost*$qty;
                }
                ?>
                <tr>
                    <th colspan="3" scope="row"></th>


                    <th>Shipping</th>
                    <td><?php echo "$".$shipping ?></td>
                </tr>
                <tr>
                            <th colspan="3" scope="row"></th>


                            <th>Total</th>
                            <td><?php echo "$".($total+$shipping) ?></td>
                        </tr>
                        </tbody>
                    </table>



                </div>
            </div>
        </div>
    </section>
    <!-- End Error Area -->

    <!-- Footer Area -->
    <?php
    include_once("inc/footer.php");
    ?>
    <!-- //Footer Area -->

</div>
<!-- //Main wrapper -->

<!-- JS Files -->
<script src="js/vendor/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/active.js"></script>

</body>
</html>