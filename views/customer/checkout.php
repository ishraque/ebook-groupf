﻿<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <?php include("inc/links.php"); ?>

	<!-- Modernizer js -->
	<script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
		
		<!-- Header -->
        <?php
        include("inc/header.php");
        ?>
		<!-- //Header -->
		<!-- Start Search Popup -->
		<div class="box-search-content search_active block-bg close__top">
			<form id="search_mini_form" class="minisearch" action="#">
				<div class="field__search">
					<input type="text" placeholder="Search entire store here...">
					<div class="action">
						<a href="#"><i class="zmdi zmdi-search"></i></a>
					</div>
				</div>
			</form>
			<div class="close__wrap">
				<span>close</span>
			</div>
		</div>
		<!-- End Search Popup -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Checkout</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="index.php">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Checkout</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- Start Checkout Area -->
        <section class="wn__checkout__area section-padding--lg bg__white">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-12">
        				<div class="wn_checkout_wrap">
        					<div class="checkout_info">
        						<span>Returning customer ?</span>
        						<a class="showlogin" href="#">Click here to login</a>
        					</div>
        					<div class="checkout_login">
        						<form class="wn__checkout__form" action="#">
        							<p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing & Shipping section.</p>

        							<div class="input__box">
        								<label>Username or email <span>*</span></label>
        								<input type="text">
        							</div>

        							<div class="input__box">
        								<label>password <span>*</span></label>
        								<input type="password">
        							</div>
        							<div class="form__btn">
        								<button>Login</button>
        								<label class="label-for-checkbox">
        									<input id="rememberme" name="rememberme" value="forever" type="checkbox">
        									<span>Remember me</span>
        								</label>
        								<a href="#">Lost your password?</a>
        							</div>
        						</form>
        					</div>
        					<div class="checkout_info">
        						<span>Have a coupon? </span>
        						<a class="showcoupon" href="#">Click here to enter your code</a>
        					</div>
        					<div class="checkout_coupon">
        						<form action="#">
        							<div class="form__coupon">
        								<input type="text" placeholder="Coupon code">
        								<button>Apply coupon</button>
        							</div>
        						</form>
        					</div>
        				</div>
        			</div>
        		</div>
        		<div class="row">


        			<div class="col-lg-6 col-12">
        				<div class="customer_details">
        					<h3>Billing details</h3>
                            <form method="post" action="checkout/process_checkout.php">
                                <div class="customar__field">
                                    <div class="margin_between">
                                        <div class="input_box space_between">
                                            <label>First name <span>*</span></label>
                                            <input name="firstName" type="text">
                                        </div>
                                        <div class="input_box space_between">
                                            <label>last name <span>*</span></label>
                                            <input name="lastName" type="text">
                                        </div>
                                    </div>
                                    <div class="input_box">
                                        <label>Company name <span>*</span></label>
                                        <input name="companyName" type="text">
                                    </div>
                                    <div class="input_box">
                                        <label>Country<span>*</span></label>
                                        <select name="country" class="select__option">
                                            <option>Select a country…</option>
                                            <option>Afghanistan</option>
                                            <option>American Samoa</option>
                                            <option>Anguilla</option>
                                            <option>American Samoa</option>
                                            <option>Antarctica</option>
                                            <option>Antigua and Barbuda</option>
                                        </select>
                                    </div>
                                    <div class="input_box">
                                        <label>Address <span>*</span></label>
                                        <input name="address" type="text" placeholder="Street address">
                                    </div>
                                    <div class="input_box">
                                        <input type="text" placeholder="Apartment, suite, unit etc. (optional)">
                                    </div>
                                    <div class="input_box">
                                        <label>District<span>*</span></label>
                                        <select name="district" class="select__option">
                                            <option>Select a country…</option>
                                            <option>Afghanistan</option>
                                            <option>American Samoa</option>
                                            <option>Anguilla</option>
                                            <option>American Samoa</option>
                                            <option>Antarctica</option>
                                            <option>Antigua and Barbuda</option>
                                        </select>
                                    </div>
                                    <div class="input_box">
                                        <label>Postcode / ZIP <span>*</span></label>
                                        <input name="zip" type="text">
                                    </div>
                                    <div class="margin_between">
                                        <div class="input_box space_between">
                                            <label>Phone <span>*</span></label>
                                            <input name="phone" type="text">
                                        </div>

                                        <div class="input_box space_between">
                                            <label>Email address <span>*</span></label>
                                            <input name="email" type="email">
                                        </div>
                                    </div>

                                    <input name="checkout" class="btn btn-dark" type="submit" value="Confirm Payment & Continue">
                                </div>
                            </form>


        					<div class="create__account">
        						<div class="wn__accountbox">
	        						<input class="input-checkbox" name="createaccount" value="1" type="checkbox">
	        						<span>Create an account ?</span>
        						</div>
        						<div class="account__field">
        							<form action="#">
        								<label>Account password <span>*</span></label>
        								<input type="text" placeholder="password">
        							</form>
        						</div>
        					</div>
        				</div>
        				<div class="customer_details mt--20">
        					<div class="differt__address">
	        					<input name="ship_to_different_address" value="1" type="checkbox">
	        					<span>Ship to a different address ?</span>
        					</div>
        					<div class="customar__field differt__form mt--40">
        						<div class="margin_between">
	        						<div class="input_box space_between">
	        							<label>First name <span>*</span></label>
	        							<input type="text">
	        						</div>
	        						<div class="input_box space_between">
	        							<label>last name <span>*</span></label>
	        							<input type="text">
	        						</div>
        						</div>
        						<div class="input_box">
        							<label>Company name <span>*</span></label>
        							<input type="text">
        						</div>
        						<div class="input_box">
        							<label>Country<span>*</span></label>
        							<select class="select__option">
										<option>Select a country…</option>
										<option>Afghanistan</option>
										<option>American Samoa</option>
										<option>Anguilla</option>
										<option>American Samoa</option>
										<option>Antarctica</option>
										<option>Antigua and Barbuda</option>
        							</select>
        						</div>
        						<div class="input_box">
        							<label>Address <span>*</span></label>
        							<input type="text" placeholder="Street address">
        						</div>
        						<div class="input_box">
        							<input type="text" placeholder="Apartment, suite, unit etc. (optional)">
        						</div>
        						<div class="input_box">
        							<label>District<span>*</span></label>
        							<select class="select__option">
										<option>Select a country…</option>
										<option>Afghanistan</option>
										<option>American Samoa</option>
										<option>Anguilla</option>
										<option>American Samoa</option>
										<option>Antarctica</option>
										<option>Antigua and Barbuda</option>
        							</select>
        						</div>
								<div class="input_box">
									<label>Postcode / ZIP <span>*</span></label>
									<input type="text">
								</div>
								<div class="margin_between">
									<div class="input_box space_between">
										<label>Phone <span>*</span></label>
										<input type="text">
									</div>
									<div class="input_box space_between">
										<label>Email address <span>*</span></label>
										<input type="email">
									</div>
								</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-6 col-12 md-mt-40 sm-mt-40">
        				<div class="wn__order__box">
        					<h3 class="onder__title">Your order</h3>
        					<ul class="order__total">
        						<li>Product</li>
        						<li>Total</li>
        					</ul>
        					<ul class="order_product">
                            <?php
                            $cart_items = $obj->showUserCart($user_id);
                            $subtotal = $obj->cartSubTotal($user_id);
                            $shipping_rate = 100;
                            foreach ($cart_items as $cart_item) {
                                extract($cart_item);
                                ?>

                                <li><?php echo $product_title ?> × <?php echo $qty ?>
                                    <span>$<?php echo $total_price ?></span></li>

                                <?php
                            }

                                ?>

<!--        						<li>Buscipit at magna × 1<span>$48.00</span></li>-->
<!--        						<li>Buscipit at magna × 1<span>$48.00</span></li>-->
<!--        						<li>Buscipit at magna × 1<span>$48.00</span></li>-->
        					</ul>
        					<ul class="shipping__method">
        						<li>Cart Subtotal <span>$<?php echo $subtotal ?></span></li>
        						<li>Shipping 
        							<ul>
        								<li>

        									<label>Flat Rate: $<?php echo $shipping_rate ?></label>

        								</li>
<!--        								<li>-->
<!--        									<input name="shipping_method[0]" data-index="0" value="legacy_flat_rate" checked="checked" type="radio">-->
<!--        									<label>Flat Rate: $--><?php //echo $subtotal ?><!--</label>-->
<!--        								</li>-->
        							</ul>
        						</li>
        					</ul>
        					<ul class="total__amount">
        						<li>Order Total <span>$<?php echo $subtotal+$shipping_rate ?></span></li>
        					</ul>
        				</div>
					    <div id="accordion" class="checkout_accordion mt--30" role="tablist">
						    <div class="payment">
						        <div class="che__header" role="tab" id="headingOne">
						          	<a class="checkout__title" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						                <span>Direct Bank Transfer</span>
						          	</a>
						        </div>
						        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
					            	<div class="payment-body">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</div>

                                </div>
						    </div>
						    <div class="payment">
						        <div class="che__header" role="tab" id="headingTwo">
						          	<a class="collapsed checkout__title" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							            <span>Cheque Payment</span>
						          	</a>
						        </div>
						        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
					          		<div class="payment-body">Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</div>
						        </div>
						    </div>
						    <div class="payment">
						        <div class="che__header" role="tab" id="headingThree">
						          	<a class="collapsed checkout__title" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							            <span>Cash on Delivery</span>
						          	</a>
						        </div>
						        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
					          		<div class="payment-body">Pay with cash upon delivery.</div>
						        </div>
						    </div>
						    <div class="payment">
						        <div class="che__header" role="tab" id="headingFour">
						          	<a class="collapsed checkout__title" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							            <span>PayPal <img src="images/icons/payment.png" alt="payment images"> </span>
						          	</a>
						        </div>
						        <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
					          		<div class="payment-body">Pay with cash upon delivery.</div>
						        </div>
						    </div>
					    </div>

        			</div>
        		</div>
        	</div>
        </section>
        <!-- End Checkout Area -->
		<!-- Footer Area -->
        <?php include("inc/footer.php"); ?>
		<!-- //Footer Area -->

	</div>
	<!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="js/vendor/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/active.js"></script>

</body>
</html>