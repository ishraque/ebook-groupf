﻿<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php include("inc/links.php"); ?>

	<!-- Modernizer js -->
	<script src="js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
		
		<!-- Header -->
<?php
include("inc/header.php");
?>
		<!-- //Header -->
		<!-- Start Search Popup -->
		<div class="box-search-content search_active block-bg close__top">
			<form id="search_mini_form" class="minisearch" action="#">
				<div class="field__search">
					<input type="text" placeholder="Search entire store here...">
					<div class="action">
						<a href="#"><i class="zmdi zmdi-search"></i></a>
					</div>
				</div>
			</form>
			<div class="close__wrap">
				<span>close</span>
			</div>
		</div>
		<!-- End Search Popup -->
        <!-- Start Bradcaump area -->
        <div class="ht__bradcaump__area bg-image--3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bradcaump__inner text-center">
                        	<h2 class="bradcaump-title">Shopping Cart</h2>
                            <nav class="bradcaump-content">
                              <a class="breadcrumb_item" href="index.php">Home</a>
                              <span class="brd-separetor">/</span>
                              <span class="breadcrumb_item active">Shopping Cart</span>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Bradcaump area -->
        <!-- cart-main-area start -->
        <div class="cart-main-area section-padding--lg bg--white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 ol-lg-12">
                        <form action="cart/process_update_cart.php" method="post">
                            <div class="table-content wnro__table table-responsive">
                                <table>
                                    <thead>
                                        <tr class="title-top">
                                            <th class="product-thumbnail">Image</th>
                                            <th class="product-name">Product</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                            <th class="product-subtotal">Total</th>
                                            <th class="product-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>


                                    <?php
                                     foreach ($cart_items as $cart_item){
                                         extract($cart_item);
                                         ?>



                                        <tr>
                                            <td class="product-thumbnail"><a href="#"><img src="../adminpanel/products/uploads/<?php echo $picture ?>" alt="<?php echo $product_title ?>"></a></td>
                                            <td class="product-name"><a href="#"><?php echo $product_title ?></a></td>
                                            <td class="product-price"><span class="amount">$<?php echo $unite_price ?></span></td>
                                            <td class="product-quantity">
                                                <input type="hidden" value="<?php echo $id ?>" name="id[]">
                                                <input type="hidden" value="<?php echo $unite_price ?>" name="unite_price[]">
                                                <input name="qty[]" type="number" value="<?php echo $qty ?>"></td>
                                            <td class="product-subtotal">$<?php echo $total_price ?></td>
                                            <td class="product-remove"><a href="cart/process_delete.php?id=<?php echo $id ?>">X</a></td>
                                        </tr>

                                   <?php
                                     }
                                    ?>




                                    </tbody>
                                </table>
                            </div>

                        <div class="cartbox__btn">
                            <ul class="cart__btn__list d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
                                <li><a href="#">Coupon Code</a></li>
                                <li><a href="#">Apply Code</a></li>
                                <li><input name="update" class="updateCart_btn" type="submit" value="Update Cart"> </a></li>
                                <li><a href="checkout.php">Check Out</a></li>
                            </ul>
                        </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 offset-lg-6">
                        <div class="cartbox__total__area">
                            <div class="cartbox-total d-flex justify-content-between">
                                <ul class="cart__total__list">
<!--                                    <li>Cart total</li>-->
                                    <li>Sub Total</li>
                                </ul>
                                <ul class="cart__total__tk">
<!--                                    <li>$--><?php //echo $subtotal ?><!--</li>-->
                                    <li>$<?php echo $subtotal ?></li>
                                </ul>
                            </div>
                            <div class="cart__total__amount">
                                <span>Grand Total</span>
                                <span>$<?php echo $subtotal ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <!-- cart-main-area end -->
		<!-- Footer Area -->
<?php include("inc/footer.php"); ?>
		<!-- //Footer Area -->

	</div>
	<!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="js/vendor/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/active.js"></script>
    <script src="js/myscript.js"></script>
</body>
</html>